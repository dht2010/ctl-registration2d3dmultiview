// simple_volume_filter.cl

// OpenCL kernel - with one additional argument
kernel void filter( read_only image3d_t inputVol,   /* the input volume */
                    global float* filteredVol,      /* the output buffer */
                    uint z,                         /* the index of the z-slice to be processed */
                    float param)                    /* our additional parameter (scaling factor) */
{
    // get IDs
    const int x = get_global_id(0);
    const int y = get_global_id(1);
    const int4 voxIdx = (int4)(x, y, z, 0);

    // read out the value of the voxel in 'inputVol'
    const float voxVal = read_imagef(inputVol, voxIdx).x; // can choose any component, since input is CL_INTENSITY

    // multiply the input voxel by 'param' and store result in 'filteredVol'
    // NOTE: we use the convenience method 'write_bufferf' here, see documentation of OpenCLFunctions::write_bufferf()
    write_bufferf(filteredVol, voxIdx, param * voxVal, inputVol); // passing 'inputVol' here provides the dimensions of the volume 
}
