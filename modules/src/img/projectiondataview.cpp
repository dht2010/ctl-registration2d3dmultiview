#include "projectiondataview.h"
#include "processing/threadpool.h"

#include <QtGlobal>
#include <limits>
#include <numeric>
#include <random>
#include <set>

namespace CTL {

/*!
 * \brief Constructs a ProjectionDataView that references the projections with ids \a viewIds in
 * \a projections.
 */
ProjectionDataView::ProjectionDataView(const ProjectionData& projections, std::vector<uint> viewIds)
    : _dataPtr(&projections)
    , _viewIds(std::move(viewIds))
{
    // check if view is required that exceeds number of views in 'projections'
    Q_ASSERT(std::find_if(_viewIds.cbegin(), _viewIds.cend(),
                          [this](uint id) { return id >= _dataPtr->nbViews(); })
             == _viewIds.cend());
}

/*!
 * \brief Constructs a ProjectionDataView that references all data of \a projections.
 *
 * This creates a ProjectionDataView containing all views in \a projections. Same as passing a
 * vector containing the numbers from zero to (number of projections - 1) to the two-argument ctor.
 */
ProjectionDataView::ProjectionDataView(const ProjectionData& projections)
    : _dataPtr(&projections)
    , _viewIds(projections.nbViews())
{
    std::iota(_viewIds.begin(), _viewIds.end(), 0u);
}

/*!
 * \brief Creates a ProjectionDataView that does not reference any data.
 *
 * Note that the resulting instance is invalid and can only be made valid through re-assignment or
 * by means of resetData(). Its sole purpose is creation of placeholder variables.
 * For reasons of expressiveness and readability, it is strongly encouraged to use the invalidView()
 * factory method (does the same thing) instead of this ctor.
 */
ProjectionDataView::ProjectionDataView()
    : _dataPtr(nullptr)
{
}

/*!
 * \brief Creates a ProjectionDataView that does not reference any data.
 *
 * Note that the resulting instance is invalid and can only be made valid through re-assignment or
 * by means of resetData(). Its sole purpose is creation of placeholder variables.
 * For reasons of expressiveness and readability, it is strongly encouraged to use this method
 * instead of the default ctor.
 */
ProjectionDataView ProjectionDataView::invalidView()
{
    return {};
}

/*!
 * \brief Creates a ProjectionDataView on temporary (r-value) \a projections.
 *
 * This allows creation of a ProjectionDataView from a temporary ProjectionData object. Note that
 * this leads to a dangling (hence, the name) reference inside the view. Using the
 * ProjectionDataView later on will most likely result in a crash.
 * However, within a single expression, this remains valid use. Therefore, it allows for
 * convenient in-line use-cases like:
 * `rec.reconstruct(ProjectionDataView::dangling(projector.project(...)))`.
 *
 * Example:
 * \code
 *  AcquisitionSetup setup(makeCTSystem<blueprints::GenericCarmCT>(), 100);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  auto P = makeProjector<OCL::RayCasterProjector>();
 *  auto R = makeReconstructor<OCL::FDKReconstructor>();
 *
 *  P->configure(setup);
 *  R->configure(setup);
 *
 *  auto phantom = VoxelVolume<float>::cube(100, 1.0f, 1.0f);
 *  auto reco = R->reconstruct(ProjectionDataView::dangling(P->project(phantom)),
 *                             VoxelVolume<float>::cube(128, 1.0f, 0.0f));
 *
 *  // note: visualization requires the 'gui_widgets.pri' submodule
 *  gui::plot(reco);
 * \endcode
 */
ProjectionDataView ProjectionDataView::dangling(ProjectionData&& projections)
{
    return ProjectionDataView(projections);
}

/*!
 * \brief Creates a ProjectionDataView on the projections with ids \a viewIds of temporary (r-value
 * reference) data \a projections.
 *
 * Same as ProjectionDataView::dangling(ProjectionData&&) just with the additional possibility to
 * select a subset of views from the referenced data through their ids (i.e. \a viewIds).
 */
ProjectionDataView ProjectionDataView::dangling(ProjectionData&& projections,
                                                std::vector<uint> viewIds)
{
    return ProjectionDataView(projections, std::move(viewIds));
}

/*!
 * \brief Resets the data that is referenced by this instance to \a projections and the
 * corresponding referenced view ids to \a viewIds.
 *
 * This is essentially the same as re-assignement with a newly created instance of
 * ProjectionDataView on the new data (i.e. \a projections and \a viewIds).
 */
void ProjectionDataView::resetData(const ProjectionData& projections, std::vector<uint> viewIds)
{
    *this = ProjectionDataView(projections, std::move(viewIds));
}

/*!
 * \brief Returns the dimensions of the referenced data under consideration that only a subset of
 * views may be referenced.
 *
 * This returns the view dimensions (i.e. number of channels, rows, and modules) of the referenced
 * data and the number of views referenced by this instance.
 *
 * Same as `ProjectionData::Dimensions(viewDimensions(), nbViews())`.
 */
ProjectionData::Dimensions ProjectionDataView::dimensions() const
{
    Q_ASSERT(isValid());
    return { _dataPtr->viewDimensions(), nbViews() };
}

/*!
 * \brief Returns a reference-to-const to the first view referenced by this instance.
 */
const SingleViewData& ProjectionDataView::first() const
{
    Q_ASSERT(isValid());
    Q_ASSERT(nbViews() > 0);
    return _dataPtr->view(_viewIds.front());
}

/*!
 * \brief Returns a reference-to-const to *i*-th view referenced by this instance.
 */
const SingleViewData& ProjectionDataView::view(uint i) const
{
    Q_ASSERT(isValid());
    Q_ASSERT(i < nbViews());
    return _dataPtr->view(_viewIds[i]);
}

/*!
 * \brief Returns the view dimensions (i.e. number of channels, rows, and modules) of the data
 * referenced by this instance.
 */
SingleViewData::Dimensions ProjectionDataView::viewDimensions() const
{
    Q_ASSERT(isValid());
    return _dataPtr->viewDimensions();
}

/*!
 * \brief Returns the maximum value that appears in all views that are referenced by this instance.
 *
 * Note that, since a ProjectionDataView can reference a subset of the original data - the maximum
 * of the ProjectionDataView instance may be less than that of the original (i.e. full) dataset.
 */
float ProjectionDataView::max() const
{
    Q_CHECK_PTR(_dataPtr);
    if(nbViews() == 0)
        return 0.0f;

    std::set<uint> uniqueViewIds(_viewIds.begin(), _viewIds.end());

    auto curMax = std::numeric_limits<float>::min();

    for(const auto id : uniqueViewIds)
    {
        const auto tmpMax = _dataPtr->view(id).max();
        if(tmpMax > curMax)
            curMax = tmpMax;
    }

    return curMax;
}

/*!
 * \brief Returns the minimum value that appears in all views that are referenced by this instance.
 *
 * Note that, since a ProjectionDataView can reference a subset of the original data - the minimum
 * of the ProjectionDataView instance may be greater than that of the original (i.e. full) dataset.
 */
float ProjectionDataView::min() const
{
    Q_CHECK_PTR(_dataPtr);
    if(nbViews() == 0)
        return 0.0f;

    std::set<uint> uniqueViewIds(_viewIds.begin(), _viewIds.end());

    auto curMin = std::numeric_limits<float>::max();

    for(const auto id : uniqueViewIds)
    {
        const auto tmpMin = _dataPtr->view(id).max();
        if(tmpMin < curMin)
            curMin = tmpMin;
    }

    return curMin;
}

/*!
 * \brief Returns a concatenated vector of all pixel values in the referenced data.
 *
 * Values are in row-major order, i.e. the vector starts with the data from the first row
 * of the first module, followed by the remaining rows of that module. Subsequently, the next
 * modules are appended with the same concept. Ultimately, proceeds with the next view that is
 * referenced by this instance.
 */
std::vector<float> ProjectionDataView::toVector() const
{
    Q_ASSERT(isValid());
    const auto nbViews = this->nbViews();
    const auto elementsPerView = viewDimensions().totalNbElements();

    std::vector<float> ret(elementsPerView * nbViews);

    auto destIt = ret.begin();

    for(auto v = 0u; v < nbViews; ++v)
    {
        auto viewVectorized = view(uint(v)).toVector();
        auto srcIt = viewVectorized.cbegin();
        std::copy_n(srcIt, elementsPerView, destIt);
        destIt += elementsPerView;
    }

    return ret;
}

/*!
 * \brief Returns \c true if this instance references the view with id \a viewId from the original
 * dataset.
 */
bool ProjectionDataView::containsView(uint viewId) const
{
    return std::find(_viewIds.cbegin(), _viewIds.cend(), viewId) != _viewIds.cend();
}

/*!
 * \brief Randomly shuffles the order in which the view ids referenced by this instance appear.
 *
 * This uses \c std::random_device to create a seed for the random number generator used to perform
 * the shuffling.
 */
void ProjectionDataView::shuffle()
{
    std::random_device rd;
    shuffle(rd());
}

/*!
 * \brief Randomly shuffles the order in which the view ids referenced by this instance appear.
 *
 * Seeds the random number generator used to perform the shuffling with \a seed.
 */
void ProjectionDataView::shuffle(uint seed)
{
    std::mt19937 rng(seed);
    std::shuffle(_viewIds.begin(), _viewIds.end(), rng);
}

/*!
 * \brief Returns a ProjectionDataView that references the same data but with a list of view ids
 * whose order has been shuffled at random.
 *
 * This uses \c std::random_device to create a seed for the random number generator used to perform
 * the shuffling.
 */
ProjectionDataView ProjectionDataView::shuffled() const
{
    std::random_device rd;
    return shuffled(rd());
}

/*!
 * \brief Returns a ProjectionDataView that references the same data but with a list of view ids
 * whose order has been shuffled at random.
 *
 * Seeds the random number generator used to perform the shuffling with \a seed.
 */
ProjectionDataView ProjectionDataView::shuffled(uint seed) const
{
    ProjectionDataView ret(*this);

    ret.shuffle(seed);

    return ret;
}

/*!
 * \brief Returns a ProjectionDataView that references the subset with indices \a idsInView of the
 * data referenced by this instance.
 *
 * Note that the view ids (i.e. \a idsInView) refer to the id within the current ProjectionDataView.
 * That means the range of valid ids to pass to this method ranges from zero to nbViews() - 1.
 *
 * Example:
 * \code
 *  ProjectionData fullProjections(10, 10, 1);
 *  fullProjections.allocateMemory(10);         // 'fullProjections' now has 10 views
 *
 *  // we create a subset that includes every other view
 *  ProjectionDataView subset(fullProjections, { 0, 2, 4, 6, 8 });
 *  qInfo() << subset.viewIds();                // output: std::vector(0, 2, 4, 6, 8)
 *
 *  // we now futher restrict the subset by selecting the second and fourth view of 'subset'
 *  auto moreRestrictedSubset = subset.subset( { 1, 3 } );
 *  // note that the ids we passed here refer to the ids in 'subset' not in the original data
 *
 *  qInfo() << moreRestrictedSubset.viewIds();  // output: std::vector(2, 6)
 * \endcode
 */
ProjectionDataView ProjectionDataView::subset(const std::vector<uint>& idsInView) const
{
    Q_ASSERT(isValid());
    Q_ASSERT(std::find_if(idsInView.cbegin(), idsInView.cend(),
                          [this](uint id) { return id >= this->nbViews(); })
             == idsInView.cend());

    std::vector<uint> subsetIds(idsInView.size());
    std::transform(idsInView.cbegin(), idsInView.cend(), subsetIds.begin(),
                   [this](uint id) { return _viewIds[id]; });

    return ProjectionDataView{ *_dataPtr, std::move(subsetIds) };
}

/*!
 * \brief Returns a ProjectionDataView that references a subset containing \a count views starting
 * at view id \a pos of the data referenced by this instance.
 *
 * Same as using the index-based version of subset(const std::vector<uint>&) with an input vector
 * of `{ pos, ..., pos + count -1 }`.
 *
 * \code
 *  ProjectionData fullProjections(10, 10, 1);
 *  fullProjections.allocateMemory(10);         // 'fullProjections' now has 10 views
 *
 *  // we create a subset that includes every other view
 *  ProjectionDataView subset(fullProjections, { 0, 2, 4, 6, 8 });
 *  qInfo() << subset.viewIds();                // output: std::vector(0, 2, 4, 6, 8)
 *
 *  // we now futher restrict the subset by selecting from 'subset' three views starting at id 1
 *  auto moreRestrictedSubset = subset.subset(1, 3);
 *
 *  qInfo() << moreRestrictedSubset.viewIds();  // output: std::vector(2, 4, 6)
 * \endcode
 */
ProjectionDataView ProjectionDataView::subset(uint pos, uint count) const
{
    Q_ASSERT(isValid());
    Q_ASSERT(pos < nbViews());

    count = std::min(count, nbViews() - pos);

    std::vector<uint> subsetIds(count);
    std::iota(subsetIds.begin(), subsetIds.end(), pos);
    std::transform(subsetIds.cbegin(), subsetIds.cend(), subsetIds.begin(),
                   [this](uint id) { return _viewIds[id]; });

    return ProjectionDataView{ *_dataPtr, std::move(subsetIds) };
}

/*!
 * \brief Returns the view ids of the views that are referenced by this instance.
 */
const std::vector<uint>& ProjectionDataView::viewIds() const { return _viewIds; }

/*!
 * \brief Returns a copy of the data referenced by this instance as ProjectionData.
 *
 * Use this method wherever you need a ProjectionData object when working with a ProjectionDataView.
 * A prominent example would be visualization using `gui::plot()`.
 *
 * The waiver of implicit conversion from ProjectionDataView to ProjectionData is done intentionally
 * to avoid the risk of unwanted copies.
 *
 * Same as explicit conversion through `operator ProjectionData()`.
 */
ProjectionData ProjectionDataView::dataCopy() const
{
    Q_ASSERT(isValid());
    return ProjectionData{ *this };
}

/*!
 * \brief Returns \c true if this instance and \a other are equal.
 *
 * Comparison operates on the level of the actual referenced data, meaning that as long as the data
 * that one would access when using the two instances is equal, both ProjectionDataView instances
 * are considered equal. For that, it is checked whether:
 * - dimensions of both instances are identical
 * - data of the referenced views is the same (using comparison operation of SingleViewData)
 *
 * Since this means, in particular, that for equality it is not mandatory that this instance and
 * \a other are identical with respect to their internal state (i.e. same data reference and same
 * view id vectors), multiple cases can exist where two instances are considered equal.
 * This encompasses two different settings:
 * 1. Both instances reference the exact same dataset (i.e. point to the same memory) and contain
 * the same ids.
 * 2. The two instances reference different datasets (and potentially also have different view id
 * vectors (with the same length)), but the actual data that is referenced is equal.
 *
 * Note: must not be applied to invalid views.
 */
bool ProjectionDataView::operator==(const ProjectionDataView& other) const
{
    if(_dataPtr == other._dataPtr && _viewIds == other._viewIds)
        return true;

    Q_ASSERT(isValid());

    if(dimensions() != other.dimensions())
        return false;

    for(auto v = 0u, nbViews = this->nbViews(); v < nbViews; ++v)
        if(this->view(v) != other.view(v))
            return false;

    return true;
}

/*!
 * \brief Returns \c true if this instance and \a other are not equal.
 *
 * See comparison operator for more details on how equality is determined.
 */
bool ProjectionDataView::operator!=(const ProjectionDataView& other) const
{
    return !(*this == other);
}

/*!
 * \brief Computes the sum of this instance and \a other and returns the result as ProjectionData.
 */
ProjectionData ProjectionDataView::operator+(const ProjectionDataView& other) const
{
    Q_ASSERT(isValid());
    Q_ASSERT(dimensions() == other.dimensions());
    if(dimensions() != other.dimensions())
        throw std::domain_error("ProjectionDataView requires same dimensions for '+' operation:\n"
                                + dimensions().info() + " + " + other.dimensions().info());

    ProjectionData ret(*this);

    if(nbViews() == 0u)
        return ret;

    auto threadTask = [&ret, &other](uint begin, uint end) {
        for(auto view = begin; view < end; ++view)
            ret.view(view) += other.view(view);
    };

    this->parallelExecution(threadTask);

    return ret;
}

/*!
 * \brief Computes the difference between this instance and \a other and returns the result as
 * ProjectionData.
 */
ProjectionData ProjectionDataView::operator-(const ProjectionDataView& other) const
{
    Q_ASSERT(isValid());
    Q_ASSERT(dimensions() == other.dimensions());
    if(dimensions() != other.dimensions())
        throw std::domain_error("ProjectionDataView requires same dimensions for '-' operation:\n"
                                + dimensions().info() + " - " + other.dimensions().info());

    ProjectionData ret(*this);

    if(nbViews() == 0u)
        return ret;

    auto threadTask = [&ret, &other](uint begin, uint end) {
        for(auto view = begin; view < end; ++view)
            ret.view(view) -= other.view(view);
    };

    this->parallelExecution(threadTask);

    return ret;
}

/*!
 * \brief Multiplies data referenced by this instance with \a factor and returns the result as
 * ProjectionData.
 */
ProjectionData ProjectionDataView::operator*(float factor) const
{
    Q_ASSERT(isValid());

    ProjectionData ret(*this);
    ret *= factor;

    return ret;
}

/*!
 * \brief Divides data referenced by this instance by \a divisor and returns the result as
 * ProjectionData.
 */
ProjectionData ProjectionDataView::operator/(float divisor) const
{
    Q_ASSERT(isValid());

    ProjectionData ret(*this);
    ret /= divisor;

    return ret;
}

/*!
 * \brief Explicit conversion to ProjectionData; returns a copy of the data referenced by this
 * instance.
 *
 * Use this method wherever you need a ProjectionData object when working with a ProjectionDataView.
 * A prominent example would be visualization using `gui::plot()`.
 *
 * The waiver of implicit conversion from ProjectionDataView to ProjectionData is done intentionally
 * to avoid the risk of unwanted copies.
 */
ProjectionDataView::operator ProjectionData() const
{
    Q_ASSERT(isValid());

    ProjectionData ret(viewDimensions());

    const auto nbViews = this->nbViews();
    ret.data().reserve(nbViews);

    for(auto v = 0u; v < nbViews; ++v)
        ret.append(view(v));

    return ret;
}

/*!
 * \brief Returns a ProjectionData copy of the data referenced by this instance that has been
 * combined according to the ModuleLayout \a layout.
 *
 * Note that combining data is meaningful only in case of multi-module projections.
 *
 * \sa ProjectionData::combined().
 */
ProjectionData ProjectionDataView::combined(const ModuleLayout& layout) const
{
    Q_ASSERT(isValid());
    const auto viewDim = viewDimensions();

    if(layout.isEmpty())
        return combined(ModuleLayout::canonicLayout(1, std::max(viewDim.nbModules, 1u)));

    SingleViewData::ModuleData::Dimensions moduleDim{ viewDim.nbChannels * layout.columns(),
                                                      viewDim.nbRows * layout.rows() };
    ProjectionData ret(moduleDim.width, moduleDim.height, 1);

    const auto nbViews = this->nbViews();
    ret.data().reserve(nbViews);

    for(auto v = 0u; v < nbViews; ++v)
        ret.append(view(v).combined(layout));

    return ret;
}

/*!
 * \brief Returns true if this instance references data.
 *
 * Note that this method cannot verify whether the referenced data is still valid. The
 * responsibility to ensure validity of the referenced data is at the side of the creator of the
 * ProjectionDataView instance.
 *
 * The only invalid ProjectionDataView object is one created via ProjectionDataView::invalidView()
 * or through the default constructor.
 */
bool ProjectionDataView::isValid() const
{
    return _dataPtr;
}

/*!
 * \brief Returns the number of views referenced by this instance.
 */
uint ProjectionDataView::nbViews() const { return static_cast<uint>(_viewIds.size()); }

/*!
 * Helper function for running tasks in parallel over views.
 */
template <class Function>
void ProjectionDataView::parallelExecution(const Function& f) const
{
    ThreadPool tp;
    const auto nbThreads = tp.nbThreads();
    const auto totalViews = nbViews();
    const auto viewsPerThread = static_cast<uint>(totalViews / nbThreads);

    auto t = 0u;
    for(; t < nbThreads - 1; ++t)
        tp.enqueueThread(f, t * viewsPerThread, (t + 1) * viewsPerThread);
    // last thread does the rest (viewsPerThread + x, with x < nbThreads)
    tp.enqueueThread(f, t * viewsPerThread, totalViews);
}

/*!
 * \fn ProjectionDataView::ProjectionDataView(ProjectionData&&)
 *
 * \brief Deleted for safety reasons. Please refer to
 * ProjectionDataView::dangling(ProjectionData&&).
 */

/*!
 * \fn ProjectionDataView::ProjectionDataView(ProjectionData&&,std::vector<uint>)
 *
 * \brief Deleted for safety reasons. Please refer to
 * ProjectionDataView::dangling(ProjectionData&&,std::vector<uint>).
 */

/*!
 * \fn ProjectionDataView::resetData(ProjectionData&&,std::vector<uint>)
 *
 * \brief Deleted for safety reasons.
 *
 * If you need to utilize temporary projection data within a ProjectionDataView, please refer to
 * ProjectionDataView::dangling(ProjectionData&&,std::vector<uint>).
 */


} // namespace CTL
