#include "chunk2d.h"

namespace CTL {

/*!
 * Constructs a Chunk2D with dimensions of \a dimensions.
 *
 * Note that this does NOT allocate memory for storage. To do so, use allocateMemory().
 */
template <typename T>
Chunk2D<T>::Chunk2D(const Dimensions& dimensions)
    : _dim(dimensions)
{
}

/*!
 * Constructs a Chunk2D with dimensions of \a dimensions and fills it with \a initValue.
 *
 * This constructor allocates memory for all elements.
 */
template <typename T>
Chunk2D<T>::Chunk2D(const Dimensions& dimensions, const T& initValue)
    : _data(size_t(dimensions.height) * size_t(dimensions.width), initValue)
    , _dim(dimensions)
{
}

/*!
 * Constructs a Chunk2D with dimensions of \a dimensions and sets its internal data to \a data.
 * This uses the move-semantics such that the Chunk2D internal data points to the original location of
 * \a data.
 *
 * This constructor fails if the number of elements in \a data does not match the specified dimensions.
 * In this case, an std::domain_error is thrown.
 */
template <typename T>
Chunk2D<T>::Chunk2D(const Dimensions& dimensions, std::vector<T>&& data)
    : _dim(dimensions)
{
    setData(std::move(data));
}

/*!
 * Constructs a Chunk2D with dimensions of \a dimensions and sets its internal data to \a data.
 * This creates a copy of \a data. If this is not required, consider using the move-semantic alternative
 * Chunk2D(const Dimensions&, std::vector<T>&&) instead.
 *
 * This constructor fails if the number of elements in \a data does not match the specified dimensions.
 * In this case, an std::domain_error is thrown.
 */
template <typename T>
Chunk2D<T>::Chunk2D(const Dimensions& dimensions, const std::vector<T>& data)
    : _dim(dimensions)
{
    setData(data);
}

/*!
 * Constructs a Chunk2D with dimensions of (\a width x \a height).
 *
 * Note that this does NOT allocate memory for storage. To do so, use allocateMemory().
 */
template <typename T>
Chunk2D<T>::Chunk2D(uint width, uint height)
    : _dim({ width, height })
{
}

/*!
 * Constructs a Chunk2D with dimensions of (\a width x \a height) and fills it with \a initValue.
 *
 * This constructor allocates memory for all elements.
 */
template <typename T>
Chunk2D<T>::Chunk2D(uint width, uint height, const T& initValue)
    : _data(size_t(height) * size_t(width), initValue)
    , _dim({ width, height })
{
}

/*!
 * Constructs a Chunk2D with dimensions of (\a width x \a height) and sets its internal data to \a data.
 * This uses the move-semantics such that the Chunk2D internal data points to the original location of
 * \a data.
 *
 * This constructor fails if the number of elements in \a data does not match the specified dimensions.
 * In this case, an std::domain_error is thrown.
 */
template <typename T>
Chunk2D<T>::Chunk2D(uint width, uint height, std::vector<T>&& data)
    : _dim({ width, height })
{
    setData(std::move(data));
}

/*!
 * Constructs a Chunk2D with dimensions of (\a width x \a height) and sets its internal data to \a data.
 * This creates a copy of \a data. If this is not required, consider using the move-semantic alternative
 * Chunk2D(const Dimensions&, std::vector<T>&&) instead.
 *
 * This constructor fails if the number of elements in \a data does not match the specified dimensions.
 * In this case, an std::domain_error is thrown.
 */
template <typename T>
Chunk2D<T>::Chunk2D(uint width, uint height, const std::vector<T>& data)
    : _dim({ width, height })
{
    setData(data);
}

/*!
 * Returns the maximum value in this instance.
 *
 * Returns zero if this data is empty.
 */
template<typename T>
T Chunk2D<T>::max() const
{
    if(allocatedElements() == 0)
        return T(0);

    return *std::max_element(_data.cbegin(), _data.cend());
}

/*!
 * Returns the minimum value in this instance.
 *
 * Returns zero if this data is empty.
 */
template<typename T>
T Chunk2D<T>::min() const
{
    if(allocatedElements() == 0)
        return T(0);

    return *std::min_element(_data.cbegin(), _data.cend());
}

/*!
 * Move-sets the internal data to \a data.
 * This uses the move-semantics such that the Chunk2D internal data points to the original location of
 * \a data.
 *
 * Throws an std::domain_error if the number of elements in \a data does not match the dimensions
 * of this chunk instance.
 */
template <typename T>
void Chunk2D<T>::setData(std::vector<T>&& data)
{
    if(!hasEqualSizeAs(data))
        throw std::domain_error("data vector has incompatible size for Chunk2D");

    _data = std::move(data);
}

/*!
 * Sets the internal data to \a data.
 * This creates a copy of \a data. If this is not required, consider using the move-semantic alternative
 * setData(std::vector<T>&&) instead.
 *
 * Throws an std::domain_error if the number of elements in \a data does not match the dimensions
 * of this chunk instance.
 */
template <typename T>
void Chunk2D<T>::setData(const std::vector<T>& data)
{
    if(!hasEqualSizeAs(data))
        throw std::domain_error("data vector has incompatible size for Chunk2D");

    _data = data;
}

/*!
 * Returns `true` if the number of elements in \a other is the same as in this instance.
 *
 */
template <typename T>
bool Chunk2D<T>::hasEqualSizeAs(const std::vector<T>& other) const
{
    return nbElements() == other.size();
}

/*!
 * Adds the data from \a other to this chunk and returns a reference to this instance.
 * Throws an std::domain_error if the dimensions of \a other and this chunk instance do not match.
 */
template <typename T>
Chunk2D<T>& Chunk2D<T>::operator+=(const Chunk2D<T>& other)
{
    if(_dim != other.dimensions())
        throw std::domain_error("Chunk2D requires same dimensions for '+' operation:\n"
                                + dimensions().info() + " += " + other.dimensions().info());

    std::transform(_data.cbegin(), _data.cend(), other._data.cbegin(), _data.begin(),
                   std::plus<T>());

    return *this;
}

/*!
 * Subtracts the data of \a other from this chunk and returns a reference to this instance.
 * Throws an std::domain_error if the dimensions of \a other and this chunk instance do not match.
 */
template <typename T>
Chunk2D<T>& Chunk2D<T>::operator-=(const Chunk2D<T>& other)
{
    if(_dim != other.dimensions())
        throw std::domain_error("Chunk2D requires same dimensions for '-' operation:\n"
                                + dimensions().info() + " -= " + other.dimensions().info());

    std::transform(_data.cbegin(), _data.cend(), other._data.cbegin(), _data.begin(),
                   std::minus<T>());

    return *this;
}

/*!
 * Multiplies the data of this chunk element-wise by \a factor and returns a reference to this instance.
 */
template <typename T>
Chunk2D<T>& Chunk2D<T>::operator*=(const T& factor)
{
    for(auto& val : _data)
        val *= factor;

    return *this;
}

/*!
 * Divides the data of this chunk element-wise by \a divisor and returns a reference to this instance.
 */
template <typename T>
Chunk2D<T>& Chunk2D<T>::operator/=(const T& divisor)
{
    for(auto& val : _data)
        val /= divisor;

    return *this;
}

/*!
 * Adds the data from \a other to this chunk and returns the result.
 * Throws an std::domain_error if the dimensions of \a other and this chunk instance do not match.
 */
template <typename T>
Chunk2D<T> Chunk2D<T>::operator+(const Chunk2D<T>& other) const
{
    Chunk2D<T> ret(*this);
    ret += other;

    return ret;
}

/*!
 * Subtracts the data of \a other from this chunk and returns the result.
 * Throws an std::domain_error if the dimensions of \a other and this chunk instance do not match.
 */
template <typename T>
Chunk2D<T> Chunk2D<T>::operator-(const Chunk2D<T>& other) const
{
    Chunk2D<T> ret(*this);
    ret -= other;

    return ret;
}

/*!
 * Multiplies the data of this chunk element-wise by \a factor and returns the result.
 */
template <typename T>
Chunk2D<T> Chunk2D<T>::operator*(const T& factor) const
{
    Chunk2D<T> ret(*this);
    ret *= factor;

    return ret;
}

/*!
 * Divides the data of this chunk element-wise by \a divisor and returns the results.
 */
template <typename T>
Chunk2D<T> Chunk2D<T>::operator/(const T& divisor) const
{
    Chunk2D<T> ret(*this);
    ret /= divisor;

    return ret;
}

/*!
 * Returns `true` if both dimensions (i.e. height and width) of \a other are identical to
 * this instance.
 *
 * \sa operator!=()
 */
inline bool Chunk2DDimensions::operator==(const Chunk2DDimensions& other) const
{
    return (width == other.width) && (height == other.height);
}

/*!
 * Returns `true` if at least one dimension (i.e. height or width) of \a other is different from
 * the dimensions of this instance.
 *
 * \sa operator==()
 */
inline bool Chunk2DDimensions::operator!=(const Chunk2DDimensions& other) const
{
    return (width != other.width) || (height != other.height);
}

/*!
 * Returns a string that contains the dimensions joined with " x ".
 */
inline std::string Chunk2DDimensions::info() const
{
    return std::to_string(width) + " x " + std::to_string(height);
}

/*!
 * Returns the total number of pixels in the chunk.
 */
inline size_t Chunk2DDimensions::totalNbElements() const
{
    return size_t(width) * size_t(height);
}

/*!
 * Returns the number of elements for which memory has been allocated. This is either zero if no
 * memory has been allocated (after instantiation with a non-allocating constructor) or equal to the
 * number of elements.
 *
 * Same as: constData().size().
 *
 * \sa nbElements(), allocateMemory().
 */
template <typename T>
size_t Chunk2D<T>::allocatedElements() const
{
    return _data.size();
}

/*!
 * Returns a constant reference to the std::vector storing the data.
 */
template <typename T>
const std::vector<T>& Chunk2D<T>::constData() const
{
    return _data;
}

/*!
 * Returns a constant reference to the std::vector storing the data.
 */
template <typename T>
const std::vector<T>& Chunk2D<T>::data() const
{
    return _data;
}

/*!
 * Returns a reference to the std::vector storing the data.
 */
template <typename T>
std::vector<T>& Chunk2D<T>::data()
{
    return _data;
}

/*!
 * Returns `true` if the number of allocated elements is equal to the total number of pixels.
 * Otherwise the function returns `false`.
 * \sa nbElements(), allocatedElements()
 */
template <typename T>
bool Chunk2D<T>::hasData() const
{
    return nbElements() == allocatedElements();
}

/*!
 * Returns the dimensions of the chunk.
 *
 * \sa Dimensions.
 */
template <typename T>
const typename Chunk2D<T>::Dimensions& Chunk2D<T>::dimensions() const
{
    return _dim;
}

/*!
 * Returns the height of the chunk. Same as: dimensions().height.
 *
 * \sa width().
 */
template <typename T>
uint Chunk2D<T>::height() const
{
    return _dim.height;
}

/*!
 * Returns the number of elements in the chunk. Note that these are not necessarily allocated
 * already.
 *
 * \sa allocatedElements().
 */
template <typename T>
size_t Chunk2D<T>::nbElements() const
{
    return size_t(_dim.width) * size_t(_dim.height);
}

/*!
 * Returns the pointer to the raw data in the std::vector. Same as data().data().
 *
 * \sa rawData() const.
 */
template <typename T>
T* Chunk2D<T>::rawData()
{
    return _data.data();
}

/*!
 * Returns the pointer to the constant raw data in the std::vector.
 *
 * \sa rawData().
 */
template <typename T>
const T* Chunk2D<T>::rawData() const
{
    return _data.data();
}

/*!
 * Returns the width of the chunk. Same as: dimensions().width.
 *
 * \sa height().
 */
template <typename T>
uint Chunk2D<T>::width() const
{
    return _dim.width;
}

/*!
 * Fills the chunk with \a fillValue. Note that this will overwrite all data stored in the chunk.
 *
 * This method allocates memory for the data if it has not been allocated before.
 */
template <typename T>
void Chunk2D<T>::fill(const T& fillValue)
{
    if(allocatedElements() != nbElements())
        allocateMemory();

    std::fill(_data.begin(), _data.end(), fillValue);
}

/*
 * Deletes the data of the chunk.
 *
 * \sa allocateMemory()
 */
template<typename T>
void Chunk2D<T>::freeMemory()
{
    _data.clear();
    _data.shrink_to_fit();
}

/*!
 * Returns a reference to the element at position (\a x, \a y) or (column, row).
 * Does not perform boundary checks!
 */
template <typename T>
typename std::vector<T>::reference Chunk2D<T>::operator()(uint x, uint y)
{
    Q_ASSERT((size_t(y) * size_t(_dim.width) + size_t(x)) < allocatedElements());
    return _data[size_t(y) * size_t(_dim.width) + size_t(x)];
}

/*!
 * Returns a constant reference to the element at position (\a x, \a y) or (column, row).
 * Does not perform boundary checks!
 */
template <typename T>
typename std::vector<T>::const_reference Chunk2D<T>::operator()(uint x, uint y) const
{
    Q_ASSERT((size_t(y) * size_t(_dim.width) + size_t(x)) < allocatedElements());
    return _data[size_t(y) * size_t(_dim.width) + size_t(x)];
}

/*!
 * Returns a true if the dimensions and data of \a other are equal to those of this chunk.
 */
template <typename T>
bool Chunk2D<T>::operator==(const Chunk2D<T>& other) const
{
    return (_dim == other._dim) && (_data == other._data);
}

/*!
 * Returns a true if either the dimensions or the data of \a other differ from those of this chunk.
 */
template <typename T>
bool Chunk2D<T>::operator!=(const Chunk2D<T>& other) const
{
    return (_dim != other._dim) || (_data != other._data);
}

/*!
 * Enforces memory allocation. This resizes the internal std::vector to the required number of
 * elements, given by the dimensions of the chunk, i.e. width x heigth.
 * As a result, allocatedElements() will return the same as nbElements().
 *
 * \sa nbElements().
 */
template <typename T>
void Chunk2D<T>::allocateMemory()
{
    _data.resize(nbElements());
}

/*!
 * Enforces memory allocation and if the current number of allocated elements is less than the
 * number of elements in the chunk, additional copies of \a initValue are appended.
 *
 * \sa allocatedElements(), allocateMemory(), fill().
 */
template <typename T>
void Chunk2D<T>::allocateMemory(const T& initValue)
{
    _data.resize(nbElements(), initValue);
}

namespace assist {

/*!
 * \brief Returns the value at position \a pos from \a chunk2D using (bi-)linear interpolation.
 */
inline float interpolate2D(const Chunk2D<float>& chunk2D, const mat::Matrix<2,1>& pos)
{
    return interpolate2D(chunk2D, pos.get<0>(), pos.get<1>());
}

/*!
 * \brief Returns the value at position (\a x, \a y) from \a chunk2D using (bi-)linear interpolation.
 */
inline float interpolate2D(const Chunk2D<float>& chunk2D, double x, double y)
{
    const auto nbPixels = chunk2D.dimensions();

    // bottom left pixel index
    const std::array<int,2> bl_pix { static_cast<int>(std::floor(x)),
                                     static_cast<int>(std::floor(y)) };
    // check if a border pixel is involved
    const bool borderIdxLow[2] = { bl_pix[0] < 0,
                                   bl_pix[1] < 0 };
    const bool borderIdxHigh[2] = { bl_pix[0] >= int(nbPixels.width) - 1,
                                    bl_pix[1] >= int(nbPixels.height) - 1 };

    const auto v00 = (borderIdxLow[0]  || borderIdxLow[1])  ? 0.0f : chunk2D(bl_pix[0],   bl_pix[1]);
    const auto v01 = (borderIdxLow[0]  || borderIdxHigh[1]) ? 0.0f : chunk2D(bl_pix[0],   bl_pix[1]+1);
    const auto v10 = (borderIdxHigh[0] || borderIdxLow[1])  ? 0.0f : chunk2D(bl_pix[0]+1, bl_pix[1]);
    const auto v11 = (borderIdxHigh[0] || borderIdxHigh[1]) ? 0.0f : chunk2D(bl_pix[0]+1, bl_pix[1]+1);

    const auto values   = mat::Matrix<2,2>( v00, v01, v10, v11 );
    const auto weightsX = mat::Matrix<1,2>( double(bl_pix[0] + 1) - x,
                                            x - double(bl_pix[0]) );
    const auto weightsY = mat::Matrix<2,1>( double(bl_pix[1] + 1) - y,
                                            y - double(bl_pix[1]) );

    return weightsX * values * weightsY;
}

} // namespace assist

} // namespace CTL
