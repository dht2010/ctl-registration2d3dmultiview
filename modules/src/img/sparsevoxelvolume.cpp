#include "sparsevoxelvolume.h"

#include "processing/range.h"
#include "voxelvolume.h"
#include <algorithm>

#include <QDebug>

namespace CTL {

// ctors
SparseVoxelVolume::SparseVoxelVolume(const VoxelSize& voxelSize, std::vector<SingleVoxel> data)
    : _data(std::move(data))
    , _voxelSize(voxelSize)
{
}

SparseVoxelVolume::SparseVoxelVolume(const VoxelSize& voxelSize)
    : SparseVoxelVolume(voxelSize, {})
{
}

// conversion to full voxel volume

void SparseVoxelVolume::paintToVoxelVolume(VoxelVolume<float>& volume) const
{
    const auto& voxSize = volume.voxelSize();
    const auto& offset  = volume.offset();
    const auto& dim     = volume.dimensions();

    const auto volumeRatio = _voxelSize.product() / voxelSize().product();

    const std::array<float, 3> volCorner { offset.x - 0.5f * float(dim.x - 1) * voxSize.x,
                                           offset.y - 0.5f * float(dim.y - 1) * voxSize.y,
                                           offset.z - 0.5f * float(dim.z - 1) * voxSize.z };

    const auto voxPos = [ &volCorner, &voxSize ] (const SingleVoxel& voxel)
    {
        const auto x = static_cast<uint>(std::round((voxel.x() - volCorner[0]) / voxSize.x));
        const auto y = static_cast<uint>(std::round((voxel.y() - volCorner[1]) / voxSize.y));
        const auto z = static_cast<uint>(std::round((voxel.z() - volCorner[2]) / voxSize.z));

        return std::array<uint, 3> { x, y, z };
    };

    const auto isValidPos = [ &dim ] (const std::array<uint, 3>& pos)
    {
        return (pos[0] < dim.x) && (pos[1] < dim.y) && (pos[2] < dim.z);
    };

    for(const auto& vox : _data)
    {
        const auto pos = voxPos(vox);
        if(isValidPos(pos))
            volume(pos[0], pos[1], pos[2]) += (vox.value() * volumeRatio);
    }
}

VoxelVolume<float> SparseVoxelVolume::toVoxelVolume() const
{
    const auto bounds = boundingBox();

    VoxelVolume<float>::Dimensions dim {
        static_cast<uint>(std::round(bounds.at(0).width() / _voxelSize.x)) + 1u,
        static_cast<uint>(std::round(bounds.at(1).width() / _voxelSize.y)) + 1u,
        static_cast<uint>(std::round(bounds.at(2).width() / _voxelSize.z)) + 1u };
    VoxelVolume<float>::Offset offset { bounds.at(0).center(),
                                        bounds.at(1).center(),
                                        bounds.at(2).center() };

    VoxelVolume<float> ret(dim, _voxelSize);
    ret.setVolumeOffset(offset);
    ret.fill(0.0f);

    paintToVoxelVolume(ret);

    return ret;
}

VoxelVolume<float> SparseVoxelVolume::toVoxelVolume(const Offset& offset) const
{
    const auto bounds = boundingBox();

    const auto reqX = static_cast<uint>
            (std::round(2.0f * std::max(offset.x - bounds[0].start(),
                                        bounds[0].end() - offset.x) / _voxelSize.x));
    const auto reqY = static_cast<uint>
            (std::round(2.0f * std::max(offset.y - bounds[1].start(),
                                        bounds[1].end() - offset.y) / _voxelSize.y));
    const auto reqZ = static_cast<uint>
            (std::round(2.0f * std::max(offset.z - bounds[2].start(),
                                        bounds[2].end() - offset.z) / _voxelSize.z));


    VoxelVolume<float>::Dimensions dim { reqX + 1u, reqY + 1u, reqZ + 1u };

    VoxelVolume<float> ret(dim, _voxelSize);
    ret.setVolumeOffset(offset);
    ret.fill(0.0f);

    paintToVoxelVolume(ret);

    return ret;
}

VoxelVolume<float> SparseVoxelVolume::toVoxelVolume(const Dimensions& dimension,
                                                    const Offset& offset) const
{
    VoxelVolume<float> ret(dimension, _voxelSize);
    ret.setVolumeOffset(offset);
    ret.fill(0.0f);

    paintToVoxelVolume(ret);

    return ret;
}

// iterators
SparseVoxelVolume::iterator SparseVoxelVolume::begin() { return _data.begin(); }

SparseVoxelVolume::const_iterator SparseVoxelVolume::begin() const { return _data.begin(); }

SparseVoxelVolume::iterator SparseVoxelVolume::end() { return _data.end(); }

SparseVoxelVolume::const_iterator SparseVoxelVolume::end() const { return _data.end(); }

SparseVoxelVolume::const_iterator SparseVoxelVolume::cbegin() const { return _data.cbegin(); }

SparseVoxelVolume::const_iterator SparseVoxelVolume::cend() const { return _data.cend(); }

SparseVoxelVolume::reverse_iterator SparseVoxelVolume::rbegin()
{
    return reverse_iterator( end() );
}

SparseVoxelVolume::const_reverse_iterator SparseVoxelVolume::rbegin() const
{
    return const_reverse_iterator( end() );
}

SparseVoxelVolume::reverse_iterator SparseVoxelVolume::rend()
{
    return reverse_iterator( begin() );
}

SparseVoxelVolume::const_reverse_iterator SparseVoxelVolume::rend() const
{
    return const_reverse_iterator( begin() );
}

SparseVoxelVolume::const_reverse_iterator SparseVoxelVolume::crbegin() const
{
    return const_reverse_iterator( cend() );
}

SparseVoxelVolume::const_reverse_iterator SparseVoxelVolume::crend() const
{
    return const_reverse_iterator( cbegin() );
}

// methods
void SparseVoxelVolume::addVoxel(const SingleVoxel& voxel) { _data.push_back(voxel); }

void SparseVoxelVolume::addVoxel(float x, float y, float z, float val)
{
    _data.emplace_back(x, y, z, val);
}

std::array<Range<float>, 3> SparseVoxelVolume::boundingBox() const
{
    const auto minMaxX = std::minmax_element(_data.cbegin(), _data.cend(),
                                             [] (const SingleVoxel& first, const SingleVoxel& second)
                                             { return first.x() < second.x(); } );
    const auto minMaxY = std::minmax_element(_data.cbegin(), _data.cend(),
                                             [] (const SingleVoxel& first, const SingleVoxel& second)
                                             { return first.y() < second.y(); } );
    const auto minMaxZ = std::minmax_element(_data.cbegin(), _data.cend(),
                                             [] (const SingleVoxel& first, const SingleVoxel& second)
                                             { return first.z() < second.z(); } );

    return { Range<float>(minMaxX.first->x(), minMaxX.second->x()),
             Range<float>(minMaxY.first->y(), minMaxY.second->y()),
             Range<float>(minMaxZ.first->z(), minMaxZ.second->z()) };
}

const std::vector<SparseVoxelVolume::SingleVoxel>& SparseVoxelVolume::data() const
{
    return _data;
}

std::vector<SparseVoxelVolume::SingleVoxel>& SparseVoxelVolume::data() { return _data; }

uint SparseVoxelVolume::nbVoxels() const { return static_cast<uint>(_data.size()); }

void SparseVoxelVolume::removeVoxel(uint i) { _data.erase(_data.begin() + i); }

const SparseVoxelVolume::SingleVoxel& SparseVoxelVolume::voxel(uint i) const { return _data[i]; }

SparseVoxelVolume::SingleVoxel& SparseVoxelVolume::voxel(uint i) { return _data.at(i); }

const SparseVoxelVolume::VoxelSize& SparseVoxelVolume::voxelSize() const { return _voxelSize; }

float SparseVoxelVolume::sparsityLevel() const
{
    const auto bounds = boundingBox();

    VoxelVolume<float>::Dimensions dim {
        static_cast<uint>(std::ceil(bounds.at(0).width() / _voxelSize.x)) + 1u,
        static_cast<uint>(std::ceil(bounds.at(1).width() / _voxelSize.y)) + 1u,
        static_cast<uint>(std::ceil(bounds.at(2).width() / _voxelSize.z)) + 1u };

    return static_cast<float>(nbVoxels()) / static_cast<float>(dim.totalNbElements());
}

float SparseVoxelVolume::sparsityLevel(const Dimensions& referenceDimension) const
{
    return static_cast<float>(nbVoxels()) / static_cast<float>(referenceDimension.totalNbElements());
}

} // namespace CTL
