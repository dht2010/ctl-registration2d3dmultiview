#include "abstractsource.h"
#include "genericsource.h"

namespace CTL {

std::unique_ptr<GenericSource> AbstractSource::toGeneric() const
{
    return GenericSource::fromOther(*this);
}

} // namespace CTL
