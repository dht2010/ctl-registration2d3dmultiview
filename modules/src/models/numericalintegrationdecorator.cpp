#include "numericalintegrationdecorator.h"

namespace CTL {

DECLARE_SERIALIZABLE_TYPE(NumericalIntegrationDecorator)

/*!
 * \brief Constructs a NumericalIntegrationDecorator that decorates \a model with the integration
 * interface of AbstractIntegrableDataModel.
 *
 * This instance takes ownership of \a model.
 *
 * Numerical integration using Simpson's rule will be carried out with sub-division of intervals
 * into \a nbIntervals sub-intervals. \a nbIntervals must be larger than zero; uses default value
 * of one otherwise.
 */
NumericalIntegrationDecorator::NumericalIntegrationDecorator(AbstractDataModel* model,
                                                             uint nbIntervals)
    : _model(model)
{
    setNbIntervals(nbIntervals);
}

/*!
 * \brief Sets the number of sub-intervals used in numerical integration. Must be larger than zero.
 *
 * If zero is passed to this method, the number of intervals will be set to its default value of one
 * instead.
 */
void NumericalIntegrationDecorator::setNbIntervals(uint nbIntervals)
{
    if(nbIntervals == 0)
        qWarning("NumericalIntegrationDecorator: number of intervals must not be zero. Using default"
                 " value (1) instead.");

    _nbIntervals = nbIntervals ? nbIntervals : 1u;
}

/*!
 * \brief Constructs a NumericalIntegrationDecorator that decorates \a model with the integration
 * interface of AbstractIntegrableDataModel.
 *
 * Numerical integration using Simpson's rule will be carried out with sub-division of intervals
 * into \a nbIntervals sub-intervals. \a nbIntervals must be larger than zero; uses default value
 * of one otherwise.
 */
NumericalIntegrationDecorator::NumericalIntegrationDecorator(std::unique_ptr<AbstractDataModel> model,
                                                             uint nbIntervals)
    : NumericalIntegrationDecorator(model.release(), nbIntervals)
{
}

/*!
 * \brief Returns the result of `valueAt()` of the decorated data model.
 */
float CTL::NumericalIntegrationDecorator::valueAt(float position) const
{
    return _model->valueAt(position);
}

AbstractIntegrableDataModel* NumericalIntegrationDecorator::clone() const
{
    return new NumericalIntegrationDecorator(*this);
}

QVariant NumericalIntegrationDecorator::parameter() const
{
    auto parMap = AbstractIntegrableDataModel::parameter().toMap();

    parMap.insert("number intervals", _nbIntervals);
    parMap.insert("model", _model->toVariant());

    return parMap;
}

void NumericalIntegrationDecorator::setParameter(const QVariant& parameter)
{
    AbstractIntegrableDataModel::setParameter(parameter);

    const auto parMap = parameter.toMap();

    if(parMap.contains("number intervals"))
    {
        auto nbInt = parMap.value("number intervals").toInt();
        if(nbInt < 0)
            qWarning("NumericalIntegrationDecorator: Trying to set negative number of intervals. "
                     "Using default value (1) instead.");
        setNbIntervals((nbInt < 0) ? 1u : static_cast<uint>(nbInt));
    }
    if(parMap.contains("model"))
        _model.reset(SerializationHelper::parseDataModel(parMap.value("model")));
}

/*!
 * \copybrief AbstractIntegrableDataModel::binIntegral
 *
 * Uses Simpson's rule (https://en.wikipedia.org/wiki/Simpson%27s_rule) to compute the bin
 * integral numerically. Sub-divides the interval into a number of sub-intervals that can be set
 * during construction or through setNbIntervals(). Higher number of intervals typically result in
 * more accurate results at the cost of higher computational effort.
 */
float NumericalIntegrationDecorator::binIntegral(float position, float binWidth) const
{
    const auto a = position - 0.5f * binWidth;
    const auto b = position + 0.5f * binWidth;
    const auto h = (b - a) / float(_nbIntervals);

    const auto x_i = [a, h] (uint i) { return a + float(i) * h; };

    float result = _model->valueAt(x_i(0)) + _model->valueAt(x_i(_nbIntervals));

    for(uint i = 1; i <= _nbIntervals-1; ++i)
        result += 2.0f * _model->valueAt(x_i(i));

    for(uint i = 1; i <= _nbIntervals; ++i)
        result += 4.0f * _model->valueAt( 0.5f * (x_i(i-1) + x_i(i)) );

    return result * h / 6.0f;
}

/*!
 * \brief Returns the number of sub-intervals used in numerical integration.
 */
uint NumericalIntegrationDecorator::nbIntervals() const { return _nbIntervals; }

} // namespace CTL
