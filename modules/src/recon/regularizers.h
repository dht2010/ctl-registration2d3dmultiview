#ifndef CTL_REGULARIZERS_H
#define CTL_REGULARIZERS_H

#include <QObject>

#include "processing/abstractvolumefilter.h"

namespace CTL {

/*!
 * \brief The IdentityRegularizer class is an implementation of AbstractVolumeFilter that does not
 * change input volumes at all; typically used as a placeholder.
 *
 * This volume filter's implementation of filter() does not alter input volumes at all. Its main
 * use is to serve as a placeholder (or default) regularizer in classes that allow for arbitrary
 * types of AbstractVolumeFilter to be used until a user defined custom class has been installed.
 */
class IdentityRegularizer : public AbstractVolumeFilter
{
    CTL_TYPE_ID(2010)

    public: void filter(VoxelVolume<float>& volume) override;
};

/*!
 * \brief The MedianFilterRegularizer class is a volume filter that applies a fraction of a median
 * filter operation.
 *
 * This filter computes the median of a voxel and its neighbors and adds a fraction of the
 * difference between the voxel's value and the median to the voxel. The fraction to be applied can
 * be specified during construction or later on through setRegularizationStrength(). Default
 * strength is 0.1 (i.e. applying 10\% of the difference).
 *
 * The voxel's neighborhood that is considered for computation of the median value can be selected
 * from:
 * - NeighborHood::NearestOnly: takes only the six direct neighbors into account [default]
 * - NeighborHood::Box3x3x3: considers all 27 voxels of a 3x3x3 box centered at the voxel of interest
 * Select the neighborhood type during construction or later on through setNeighborHoodType().
 *
 * Example:
 * \code
 *  // generata a volume with random data from [0, 1]
 *  auto volume = VoxelVolume<float>::cube(100, 1.0f, 0.0);
 *  std::mt19937 rng;
 *  std::uniform_real_distribution<float> dist(0.0, 1.0);
 *  std::generate(volume.begin(), volume.end(), [&rng, &dist]() { return dist(rng); });
 *
 *  // compute RMSE of the original volume
 *  qInfo() << metric::RMSE(volume, VoxelVolume<float>::cube(100, 1.0f, 0.0));      // output: 0.577281
 *
 *  // create two copies of our random volume
 *  auto volCopy = volume;
 *  auto volCopy2 = volume;
 *
 *  // filter 'volCopy' with a default constructed MedianFilterRegularizer
 *  MedianFilterRegularizer filt;
 *  filt.filter(volCopy);
 *
 *  qInfo() << metric::RMSE(volCopy, VoxelVolume<float>::cube(100, 1.0f, 0.0));     // output: 0.56618
 *
 *  // filter 'volCopy2' with a higher strength and a 3x3x3 neighborhood
 *  filt.setRegularizationStrength(1.0f);
 *  filt.setNeighborHoodType(MedianFilterRegularizer::Box3x3x3);
 *  filt.filter(volCopy2);
 *
 *  qInfo() << metric::RMSE(volCopy2, VoxelVolume<float>::cube(100, 1.0f, 0.0));    // output: 0.509319
 * \endcode
 */
class MedianFilterRegularizer : public AbstractVolumeFilter
{
    CTL_TYPE_ID(2011)
    Q_GADGET

    public: void filter(VoxelVolume<float>& volume) override;

public:
    enum NeighborHood { Box3x3x3, NearestOnly };
    Q_ENUM(NeighborHood)

    explicit MedianFilterRegularizer(float strength = 0.1f, NeighborHood neighbors = NearestOnly);

    void setNeighborHoodType(NeighborHood neighbors);
    void setRegularizationStrength(float strength);

    // AbstractVolumeFilter interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _strength = 0.1f;
    NeighborHood _neighbors = NearestOnly;

    void applyMedian3(VoxelVolume<float>& volume);
    void applyMedianNN(VoxelVolume<float>& volume);

    static QMetaEnum metaEnum();
};

/*!
 * \brief The TVRegularizer class provides an approximation of a total variation (TV) minimizing
 * regularizer.
 *
 * This is the CPU version of the TVRegularizer; for the GPU version, see OCL::TVRegularizer.
 *
 * This class implements an anisotropic version of total variation (TV) regularization.
 *
 * The strength of the regularization effect is controlled via the regularization parameter. This
 * defines the maximum change in multiples of 10 Hounsfield units (HU) that can be applied to a
 * single voxel in one execution of filter().
 *
 * This change is applied proportionally, depending on the average number of neighbors of that voxel
 * that have a positive / negative difference to the voxel. Hence, if all surrounding voxels have
 * higher/lower values than the voxel of interest, its value will be increased/decreased by
 * \a regularizationStrength * 10 HU. If the tendency of neighbor values is mixed, only the
 * corresponding fraction of change is applied, respectively.
 * The voxel's neighborhood that is considered for this operation can be selected from:
 * - NeighborHood::NearestOnly: takes only the six direct neighbors into account; in this setting,
 * all considered neighbors are weighted equally [default]
 * - NeighborHood::Box3x3x3: considers all 26 neighbors of a 3x3x3 box centered at the voxel of
 * interest; in this setting, differences will be weighted wrt. the distance of the neighbor to the
 * center voxel.
 * Select the neighborhood type during construction or later on through setNeighborHoodType().
 */
class TVRegularizer : public AbstractVolumeFilter
{
    CTL_TYPE_ID(2012)
    Q_GADGET

    public: void filter(VoxelVolume<float>& volume) override;

public:
    enum NeighborHood { Box3x3x3, NearestOnly };
    Q_ENUM(NeighborHood)

    explicit TVRegularizer(float maxChangeIn10HU = 1.0f, NeighborHood neighbors = NearestOnly);

    void setNeighborHoodType(NeighborHood neighbors);
    void setRegularizationStrength(float maxChangeIn10HU);    // max change in 10 HU

    // AbstractVolumeFilter interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    static constexpr float _step = 0.02269f / 1000.0f; // ~1 HU (at 50 keV)
    float _strength = 10.0f;                           // default 10 HU max. change
    NeighborHood _neighbors = NearestOnly;

    void applyTV3(VoxelVolume<float>& volume);
    void applyTVNN(VoxelVolume<float>& volume);

    static QMetaEnum metaEnum();
};

/*!
 * \brief The HuberRegularizer class is a regularizer based on the Huber potential function.
 *
 * This is the CPU version of the HuberRegularizer; for the GPU version, see OCL::HuberRegularizer.
 *
 * This regularizer uses the so-called Huber potential, which consists of a quadratic part that
 * transitions into a linear part for arguments whose absolute value exceeds a defined threshold
 * (called Huber edge here). Consequently, voxel differences (wrt. absolute value) below the
 * threshold contribute to the gradient linearly wrt. their difference. Differences above said
 * threshold are capped by that threshold value, thus, resulting in a TV-like contribution to the
 * regularization.
 *
 * The regularizer can be configured through a set of parameters, with the following effects:
 * - \a regularizationStrength linearly scales the portion of the gradient that is applied to change
 * a single voxel's value in one execution of filter()
 * [1.0 means 1% of the gradient is applied].
 * - \a huberEdge defines the (absolute) difference threshold in multiples of 100 HU (at 50 keV) to
 * which differences will be clamped for computation of the gradient. For differences above the
 * threshold, the regularization effect is comparable to TV minimization
 * [1.0 means the Huber edges are +-100 HU].
 * - \a weightZ and \a weightXY specify the relative strength of the regularization in Z- and XY-
 * plane, respectively (in most settings, this refers to between-plane and in-plane regularization)
 * [\a weightZ = \a weightXY means all dimensions are treated equally].
 * - \a directZNeighborWeight is a separate multiplicative weight factor for the two voxels that are
 * direct Z-neighbors of the voxel of interest
 * [1.0 means the direct Z-Neighbors are treated the same as all other Z-Neighbors].
 *
 * Parameters can be set during construction or later on through their specific setters.
 */
class HuberRegularizer : public AbstractVolumeFilter
{
    CTL_TYPE_ID(2013)

    public: void filter(VoxelVolume<float>& volume) override;

public:
    explicit HuberRegularizer(float strength = 1.0f, float huberEdgeIn100HU = 1.0,
                              float weightZ = 1.0f, float weightXY = 1.0f,
                              float directZweight = 1.0f);

    void setRegularizationStrength(float strength);
    void setHuberEdge(float edgeIn100HU);
    void setRelativeWeighting(float weightZ, float weightXY = 1.0f);
    void setDirectZNeighborWeight(float weight);

    // AbstractVolumeFilter interface
    QVariant parameter() const override;
    void setParameter(const QVariant& parameter) override;

private:
    float _strength = 0.01f;
    float _huberEdge = 100.0f * 0.02269f/1000.0f;
    float _betaZ = 1.0f;
    float _betaXY = 1.0f;
    float _directZ = 1.0f;
};

namespace assist {
constexpr float clamp(float val, float low, float high)
{
    return (val<low) ? low : ((val>high) ? high : val);
}
} // namespace assist

} //namespace CTL

#endif // CTL_REGULARIZERS_H
