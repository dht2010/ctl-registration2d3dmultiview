#include "abstractreconstructor.h"

#include "img/compositevolume.h"
#include "img/sparsevoxelvolume.h"
#include "img/spectralvolumedata.h"
#include "io/messagehandler.h"

namespace CTL {

/*!
 * \fn void AbstractReconstructor::configure(const AcquisitionSetup& setup)
 *
 * \brief Configures the reconstructor.
 *
 * This method must be implemented by sub-classes.
 * It should be used to gather all necessary information on the acquisition that are required in the
 * actual reconstruction procedure. This usually contains all geometry and system information, which
 * can be retrieved from \a setup.
 *
 * Note that the passed \a setup must always refer to the entire set of projections that will be
 * passed to a reconstruction task later on. That means, even if the passed data (to reconstruction)
 * will be a ProjectionDataView that refers only to a subset of the underlying data, the setup
 * passed in configure() must always correspond to the full projection data (i.e. the data
 * referenced by the view).
 *
 * If you intend to call configure() and any reconstruction method directly after each other, you
 * should consider using configureAndReconstruct() or configureAndReconstructTo() instead.
 */

/*!
 * \brief Returns true if the reconstruction method can be applied to data whose acquisition is
 * described by \a setup.
 *
 * Re-implement this method in sub-classes to provide reasonable information whether or not the
 * implemented method can be applied to a setup as passed. Decision criteria might include, for
 * example, restrictions on the system used (e.g. detector geometry) or geometrical limitations
 * w.r.t. the scan orbit.
 *
 * By default, this always returns \c true.
 */
bool AbstractReconstructor::isApplicableTo(const AcquisitionSetup&) const { return true; }

/*!
 * \brief Reconstruct data (in-place) from \a projections into \a volume.
 *
 * Re-implement this method in sub-classes to provide actual reconstruction functionality for
 * reconstructions operating on VoxelVolume<float>.
 * The implementation shall treat \a volume as an initialization. That means, reconstructed data
 * shall be added to the input volume (instead of overwriting its values).
 *
 * Note that \a projections is a ProjectionDataView, meaning it may contain a subset of data (or
 * data in shuffled order). Make sure to correctly address individual views by their particular
 * view ids (see ProjectionDataView::viewIds()). This becomes particularly important when referring
 * to the corresponding AcquisitionSetup. You might consider using AcquisitionSetup::subset() along
 * with the view ids of \a projections to select the correct data from setup.
 *
 * By default, throws a \c std::runtime_error stating that the functionality is not implemented.
 */
bool AbstractReconstructor::reconstructToPlain(const ProjectionDataView&, VoxelVolume<float>&)
{
    throw std::runtime_error("No reconstruction method for VoxelVolume implemented.");
}

/*!
 * \brief Reconstruct data (in-place) from \a projections into \a volume.
 *
 * Re-implement this method in sub-classes to provide actual reconstruction functionality for
 * reconstructions operating on SpectralVolumeData.
 * The implementation shall treat \a volume as an initialization. That means, reconstructed data
 * shall be added to the input volume (instead of overwriting its values).
 *
 * Note that \a projections is a ProjectionDataView, meaning it may contain a subset of data (or
 * data in shuffled order). Make sure to correctly address individual views by their particular
 * view ids (see ProjectionDataView::viewIds()). This becomes particularly important when referring
 * to the corresponding AcquisitionSetup. You might consider using AcquisitionSetup::subset() along
 * with the view ids of \a projections to select the correct data from setup.
 *
 * By default, throws a \c std::runtime_error stating that the functionality is not implemented.
 */
bool AbstractReconstructor::reconstructToSpectral(const ProjectionDataView&, SpectralVolumeData&)
{
    throw std::runtime_error("No reconstruction method for SpectralVolumeData implemented.");
}

/*!
 * \brief Reconstruct data (in-place) from \a projections into \a volume.
 *
 * Re-implement this method in sub-classes to provide actual reconstruction functionality for
 * reconstructions operating on (sub-classes of) AbstractDynamicVolumeData. You might want to
 * check for the specific sub-class and decide whether or not your implementation can handle it.
 * The implementation shall treat \a volume as an initialization. That means, reconstructed data
 * shall be added to the input volume (instead of overwriting its values).
 *
 * Note that \a projections is a ProjectionDataView, meaning it may contain a subset of data (or
 * data in shuffled order). Make sure to correctly address individual views by their particular
 * view ids (see ProjectionDataView::viewIds()). This becomes particularly important when referring
 * to the corresponding AcquisitionSetup. You might consider using AcquisitionSetup::subset() along
 * with the view ids of \a projections to select the correct data from setup.
 *
 * By default, throws a \c std::runtime_error stating that the functionality is not implemented.
 */
bool AbstractReconstructor::reconstructToDynamic(const ProjectionDataView&,
                                                 AbstractDynamicVolumeData&)
{
    throw std::runtime_error("No reconstruction method for dynamic volumes implemented.");
}

/*!
 * \brief Reconstruct data (in-place) from \a projections into \a volume.
 *
 * Re-implement this method in sub-classes to provide actual reconstruction functionality for
 * reconstructions operating on CompositeVolume.
 *
 * Note that \a projections is a ProjectionDataView, meaning it may contain a subset of data (or
 * data in shuffled order). Make sure to correctly address individual views by their particular
 * view ids (see ProjectionDataView::viewIds()). This becomes particularly important when referring
 * to the corresponding AcquisitionSetup. You might consider using AcquisitionSetup::subset() along
 * with the view ids of \a projections to select the correct data from setup.
 *
 * By default, throws a \c std::runtime_error stating that the functionality is not implemented.
 */
bool AbstractReconstructor::reconstructToComposite(const ProjectionDataView&, CompositeVolume&)
{
    throw std::runtime_error("No reconstruction method for CompositeVolume implemented.");
}

/*!
 * \brief Reconstruct data (in-place) from \a projections into \a volume.
 *
 * Re-implement this method in sub-classes to provide actual reconstruction functionality for
 * reconstructions operating on SparseVoxelVolume.
 *
 * Note that \a projections is a ProjectionDataView, meaning it may contain a subset of data (or
 * data in shuffled order). Make sure to correctly address individual views by their particular
 * view ids (see ProjectionDataView::viewIds()). This becomes particularly important when referring
 * to the corresponding AcquisitionSetup. You might consider using AcquisitionSetup::subset() along
 * with the view ids of \a projections to select the correct data from setup.
 *
 * By default, throws a \c std::runtime_error stating that the functionality is not implemented.
 */
bool AbstractReconstructor::reconstructToSparse(const ProjectionDataView&, SparseVoxelVolume&)
{
    throw std::runtime_error("No reconstruction method for SparseVoxelVolume implemented.");
}

/*!
 * \brief Returns the parameters of this instance as a QVariant.
 *
 * Re-implement this method within your sub-class such that it encapsulates all necessary
 * information into a QVariant.
 *
 * Best practice is to invoke the base class version of this method to take care
 * of all content originating from underlying base classes.
 *
 * A typical reimplementation in sub-classes might look like this:
 * \code
 * QVariantMap ret = DirectBaseClass::parameter().toMap();
 *
 * ret.insert("my new parameter", _myNewParameter);
 *
 * return ret;
 * \endcode
 */
QVariant AbstractReconstructor::parameter() const { return QVariant(); }

/*!
 * \brief Sets the parameters of this instance based on the passed QVariant \a parameter.
 *
 * Extract all necessary information contained in the passed QVariant and re-implement this method
 * within your sub-class to include all necessary additions.
 *
 * Best practice is to invoke the base class version of this method to take care of all content
 * originating from underlying base classes.
 *
 * A typical reimplementation in sub-classes might look like this:
 * \code
 * DirectBaseClass::setParameter(parameter);
 *
 * // assuming our class has a parameter member "double _myNewParameter"
 * _myNewParameter = parameter.toMap().value("my new parameter").toDouble();
 * \endcode
 */
void AbstractReconstructor::setParameter(const QVariant&) {}

/*!
 * \brief Convenience method wrapping up all reconstructToXXX() methods.
 *
 * Specialization for VoxelVolume<float> input. Directs the call to reconstructToPlain().
 *
 * \sa reconstructTo(const ProjectionDataView&,VolumeType&)
 */
template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          VoxelVolume<float>& targetVolume)
{
    return reconstructToPlain(projections, targetVolume);
}

/*!
 * \brief Convenience method wrapping up all reconstructToXXX() methods.
 *
 * Specialization for SpectralVolumeData input. Directs the call to reconstructToSpectral().
 *
 * \sa reconstructTo(const ProjectionDataView&,VolumeType&)
 */
template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          SpectralVolumeData& targetVolume)
{
    return reconstructToSpectral(projections, targetVolume);
}

/*!
 * \brief Convenience method wrapping up all reconstructToXXX() methods.
 *
 * Specialization for CompositeVolume input. Directs the call to reconstructToComposite().
 *
 * \sa reconstructTo(const ProjectionDataView&,VolumeType&)
 */
template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          CompositeVolume& targetVolume)
{
    return reconstructToComposite(projections, targetVolume);
}

/*!
 * \brief Convenience method wrapping up all reconstructToXXX() methods.
 *
 * Specialization for SparseVoxelVolume input. Directs the call to reconstructToSparse().
 *
 * \sa reconstructTo(const ProjectionDataView&,VolumeType&)
 */
template <>
bool AbstractReconstructor::reconstructTo(const ProjectionDataView& projections,
                                          SparseVoxelVolume& targetVolume)
{
    return reconstructToSparse(projections, targetVolume);
}

/*!
 * \brief Combines calls to configure() and reconstructTo().
 *
 * Specialization for VoxelVolume<float> input.  Returns the result of reconstructToPlain().
 */
template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      VoxelVolume<float>& targetVolume)
{
    configure(setup);
    return reconstructToPlain(projections, targetVolume);
}

template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      SpectralVolumeData& targetVolume)
{
    configure(setup);
    return reconstructToSpectral(projections, targetVolume);
}

template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      CompositeVolume& targetVolume)
{
    configure(setup);
    return reconstructToComposite(projections, targetVolume);
}

template <>
bool AbstractReconstructor::configureAndReconstructTo(const AcquisitionSetup& setup,
                                                      const ProjectionDataView& projections,
                                                      SparseVoxelVolume& targetVolume)
{
    configure(setup);
    return reconstructToSparse(projections, targetVolume);
}

/// \brief Returns a pointer to the notifier of the reconstructor.
///
/// The notifier object can be used to emit the signal ReconstructorNotifier::progress(qreal)
/// to broadcast an update on the (relative) progress of the reconstruction procedure. A value of 1.0
/// should indicate that the reconstruction has finished. More general information can be
/// communicated through the information() signal.
///
/// To receive emitted signals, use Qt's connect() method to connect the notifier object to any
/// receiver object of choice. The notifier can also conveniently be connected to the MessageHandler
/// of the CTL using connectToMessageHandler() or directly from the projector through
/// AbstractProjector::connectNotifierToMessageHandler().
///
/// Example - sending reconstruction progress information to a QProgressBar:
/// \code
/// AbstractReconstructor* myReconstructor /* = new ActualReconstructor */;
///
/// QProgressBar* myProgressBar = new QProgressBar();
/// // ... e.g. add the progress bar somewhere in your GUI
///
/// QObject::connect(myReconstructor->notifier(), SIGNAL(projectionFinished(int)),
///                  myProgressBar, SLOT(setValue(int)));
/// \endcode
///
/// /// Example 2 - forwarding the information text to a QStatusBar
/// \code
/// AbstractReconstructor* myReconstructor /* = new ActualReconstructor */;
///
/// QStatusBar* myStatusBar = new QStatusBar;
/// // ... e.g. add the status bar somewhere in your GUI (or take a pointer to an existing one)
///
/// QObject::connect(myReconstructor->notifier(), SIGNAL(information(QString)),
///                  myStatusBar, SLOT(showMessage(QString)));
/// \endcode
ReconstructorNotifier* AbstractReconstructor::notifier() { return _notifier.get(); }

/*!
 * \brief Connects this instance's notifier to the CTL's MessageHandler.
 *
 * Same as:
 * \code
 * notifier()->connectToMessageHandler(includeProgress);
 * \endcode
 *
 * \sa AbstractReconstructor::connectToMessageHandler
 */
void AbstractReconstructor::connectNotifierToMessageHandler(bool includeProgress)
{
    notifier()->connectToMessageHandler(includeProgress);
}

/*!
 * \brief Sets the contents of the object based on the QVariant \a variant.
 *
 * Implementation of the deserialization interface.
 * This method uses setParameter() to deserialize class members.
 */
void AbstractReconstructor::fromVariant(const QVariant& variant)
{
    SerializationInterface::fromVariant(variant);

    const auto map = variant.toMap();

    setParameter(map.value(QStringLiteral("parameters")).toMap());
}

/*!
 * \brief Stores the contents of this instance in a QVariant.
 *
 * Implementation of the serialization interface. Stores the object's type-id (from
 * SerializationInterface::toVariant()).
 *
 * This method uses parameter() to serialize class members.
 */
QVariant AbstractReconstructor::toVariant() const
{
    QVariantMap ret = SerializationInterface::toVariant().toMap();

    ret.insert(QStringLiteral("parameters"), parameter());

    return ret;
}

/*!
 * \brief Connects this instance's signals to the CTL's MessageHandler.
 *
 * This connects the information signal of this instance to the MessageHandler::messageFromSignal
 * slot, such that it processes incoming information signals as regular messages.
 * If \a includeProgress = \c true, this also establishes a connection between this instance's
 * progress() signal and the message handler. Each `progress` signal results in a message of format
 * "Reconstruction progress %1 %.", where %1 is replaced with the progress reported by the signal
 * (times 100 to express percentage).
 *
 * The method ensures that no multiple connections are established, even if it is called multiple
 * times.
 *
 * To (fully) disconnect this instance from the message handler, use:
 * \code
 * this->disconnect(&MessageHandler::instance());
 * \endcode
 */
void ReconstructorNotifier::connectToMessageHandler(bool includeProgress)
{
    QObject::connect(this, &ReconstructorNotifier::information,
                     &MessageHandler::instance(), &MessageHandler::messageFromSignal,
                     Qt::UniqueConnection);

    if(includeProgress)
    {
        const auto progressString = [] (qreal prog)
        {
            MessageHandler::instance().messageFromSignal(
                        QStringLiteral("Reconstruction progress %1 %.").arg(prog * 100));
        };
        QObject::disconnect(_progressConnection); // avoid multiple connections
        _progressConnection = QObject::connect(this, &ReconstructorNotifier::progress,
                                               &MessageHandler::instance(), progressString);
    }
}

/*!
 * \fn void ReconstructorNotifier::progress(qreal progress)
 *
 * Signal that is emitted to communicate the (relative) progress of the reconstruction procedure. A
 * value of \a progress = 1.0 means reconstruction has finished.
 */

/*!
 * \fn void ReconstructorNotifier::information(QString info)
 *
 * Signal that can be emitted to communicate a status information.
 */

/*!
 * \fn AbstractReconstructor::~AbstractReconstructor()
 *
 * Virtual default destructor.
 */
} // namespace CTL
