#include "abstractsubsetgenerator.h"

#include <sstream>

namespace CTL {


/*!
 * \fn AbstractSubsetGenerator::generateSubsetsImpl(uint iteration) const
 * \brief Implementation of the generation algorithm for subsets in the \a iteration -th iteration.
 *
 * This (pure virtual) method must be implemented in sub-classes to generate the subsets that shall
 * be used in iteration number \a iteration. The specific algorithm may also be independent of the
 * actual iteration number.
 *
 * The returned std::vector<ProjectionDataView> must contain one ProjectionDataView for each subset,
 * where each ProjectionDataView element must contain the ids of the projections that have been
 * selected for that particular subset. Note that random permutation of the subsets does not need to
 * be done in this implementation, as it is provided by shuffleSubsets().
 *
 * This method relies on data previously set through setProjections(), so consider adding a check
 * whether or not appropriate data is available. In case that your subset generation routine also
 * requires information about the acquisition - and thus, setSetup() must have been called with
 * appropriate data earlier - you should consider checking for consistency (i.e. between setup and
 * projcetions) here as well.
 */

/*!
 * \brief Constructs an AbstractSubsetGenerator.
 *
 * This ctor is protected. Seeds the RNG with the default seed (i.e. 42).
 */
AbstractSubsetGenerator::AbstractSubsetGenerator()
{
    _rng.seed(_rngSeed);
}

/*!
 * \brief Randomly permutes the order of the subsets in \a subsets.
 *
 * This permutes the order in which subsets appear in \a subsets. Uses std::shuffle in combination
 * with the RNG member of this class.
 *
 * Shuffling subset order may be beneficial in iterative reconstructions to avoid image artifacts
 * as a result of (overly) structured update schemes.
 */
void AbstractSubsetGenerator::shuffleSubsets(std::vector<ProjectionDataView>& subsets) const
{
    std::shuffle(subsets.begin(), subsets.end(), _rng);
}

// use base class documentation
void AbstractSubsetGenerator::fromVariant(const QVariant& variant)
{
    QVariantMap varMap = variant.toMap();

    setRandomGeneratorSeed(varMap.value(QStringLiteral("rng seed"), 42).toUInt());
    setSubsetPermutationEnabled(varMap.value(QStringLiteral("permute subsets"), true).toBool());

    // restore RNG state
    std::istringstream rngStateStream(varMap.value(QStringLiteral("rng state")).toString().toStdString());
    rngStateStream >> _rng;
}

// use base class documentation
QVariant AbstractSubsetGenerator::toVariant() const
{
    QVariantMap ret = SerializationInterface::toVariant().toMap();

    // encode RNG state
    std::ostringstream rngStateStream;
    rngStateStream << _rng;

    ret.insert(QStringLiteral("rng seed"), _rngSeed);
    ret.insert(QStringLiteral("rng state"), QString::fromStdString(rngStateStream.str()));
    ret.insert(QStringLiteral("permute subsets"), _permuteSubsets);

    return ret;
}

/*!
 * \brief Sets the projections that this instance shall operate on to \a projections.
 *
 * This takes a ProjectionDataView to efficiently create subsets without overhead of data copies.
 * Note that the underlying ProjectionData (i.e. the ones the view refers to) must remain valid
 * throughout the entire use time of this instance. That means that all subsets created by this
 * instance (w.r.t. the data from \a projections) are valid only as long as the ProjectionData
 * \a projections refers to is valid.
 */
void AbstractSubsetGenerator::setProjections(ProjectionDataView projections)
{
    _fullProjections = std::move(projections);
}

/*!
 * \fn AbstractSubsetGenerator::setSetup(const AcquisitionSetup& setup)
 * \brief Sets the AcquisitionSetup that belongs to the projection data to \a setup.
 *
 * \a setup must be the AcquisitionSetup that describes the acquisition of the projection data
 * passed to setProjections(). Please note that \a setup must always represent the full
 * ProjectionData that is managed by the view passed to setProjections(). That means if the
 * ProjectionDataView passed to setProjections() is already a subset, the setup passed here must
 * still refer to the full ProjectionData the view belongs to.
 */

/*!
 * \brief Sets the projections to \a projections and the corresponding acquisition setup to \a setup.
 *
 * This is a convenience method that combines the calls to setProjections() and setSetup() into a
 * single call. Pay attention that both passed parameter need to be correctly related to each other,
 * i.e. \a setup must be the AcquisitionSetup that describes the acquisition of the projection data
 * that \a projections contains.
 *
 * Please note that \a setup must always represent the full ProjectionData that is managed by
 * \a projections. That means if \a projections is already a subset, the setup passed here must
 * still refer to the full ProjectionData the view belongs to.
 *
 * \sa setProjections(), setSetup().
 */
void AbstractSubsetGenerator::setData(ProjectionDataView projections, const AcquisitionSetup& setup)
{
    setProjections(std::move(projections));
    setSetup(setup);
}

/*!
 * \brief Generates the subsets for iteration \a iteration.
 *
 * This calls generateSubsetsImpl() with the passed \a iteration and executes shuffleSubsets() on
 * the generated subsets if isSubsetPermutationEnabled() == \c true. In its default implementation,
 * this randomly permutes the order of the subsets.
 *
 * Note that data must have been set earlier. This refers to a ProjectionDataView to operate on
 * (see setProjections()) and potentially - if required by the specific sub-class - a corresponding
 * AcquisitionSetup (see setSetup()).
 */
std::vector<ProjectionDataView> AbstractSubsetGenerator::generateSubsets(uint iteration) const
{
    auto subsets = generateSubsetsImpl(iteration);

    if(_permuteSubsets)
        shuffleSubsets(subsets);

    return subsets;
}

/*!
 * \brief Convenience method to generate subsets for a certain number of iterations.
 *
 * This is a convenience wrapper method that executes generateSubsets() for all iteration numbers
 * from zero to \a nbIterations-1 and appends the resulting subsets to a std::vector.
 *
 * This is a virtual method and as such, it can be re-implemented in sub-classes if a more
 * appropriate (or more efficient) way of generating multiple sets of subsets is available.
 * However, it is strongly recommended, not to change the general behavior of the method, i.e. the
 * elements of the returned vector should always contain the subsets of the corresponding iteration
 * as if they would have been created from an isolated call to generateSubsets() with the particular
 * iteration number.
 */
std::vector<std::vector<ProjectionDataView>>
AbstractSubsetGenerator::generateAllSubsets(uint nbIterations)
{
    std::vector<std::vector<ProjectionDataView>> ret;
    ret.reserve(nbIterations);

    for(uint it = 0; it < nbIterations; ++it)
        ret.push_back(generateSubsets(it));

    return ret;
}

/*!
 * \brief Returns \c true if subset permutation is enabled; \c false otherwise
 */
bool AbstractSubsetGenerator::isSubsetPermutationEnabled() const { return _permuteSubsets; }

/*!
 * \brief Returns the ProjectionDataView that has been set via setProjections().
 */
const ProjectionDataView& AbstractSubsetGenerator::projections() const { return _fullProjections; }

/*!
 * \brief Returns the seed that has been used to seed the RNG of this instance.
 *
 * Note that this only returns the original seed. The internal state of the RNG might be different
 * from the point it was seeded. For that reason, the full state of the RNG will be serialized in
 * toVariant() for the purpose of later restoration (i.e. deserialization).
 */
uint AbstractSubsetGenerator::randomGeneratorSeed() const { return _rngSeed; }

/*!
 * \brief Sets the seed of the RNG to \a seed and actually seeds the RNG.
 */
void AbstractSubsetGenerator::setRandomGeneratorSeed(uint seed)
{
    _rngSeed = seed;
    _rng.seed(_rngSeed);
}

/*!
 * \brief Sets the use of subset permutation to \a enabled.
 *
 * This enables/disables subset permutation. To be more precise, if \a enabled=\c true, the method
 * shuffleSubsets() will be applied to the generated subset when generateSubsets() is called. In its
 * default implementation, shuffleSubsets() randomly permutes the order of the subsets.
 */
void AbstractSubsetGenerator::setSubsetPermutationEnabled(bool enabled)
{
    _permuteSubsets = enabled;
}

/*!
 * \brief Creates an AbstractFixedSizeSubsetGenerator and sets the number of subsets to
 * \a nbSubsets.
 */
AbstractFixedSizeSubsetGenerator::AbstractFixedSizeSubsetGenerator(uint nbSubsets)
    :_nbSubsets(nbSubsets)
{
}

/*!
 * \brief Returns the number of subsets that are generated.
 */
uint AbstractFixedSizeSubsetGenerator::nbSubsets() const { return _nbSubsets; }

/*!
 * \brief Sets the number of subsets that will be generated to \a nbSubsets.
 */
void AbstractFixedSizeSubsetGenerator::setNbSubsets(uint nbSubsets)
{
    if(nbSubsets == 0)
        throw std::domain_error("AbstractFixedSizeSubsetGenerator::setNbSubsets: "
                                "Number of subsets must be larger than zero.");
    _nbSubsets = nbSubsets;
}

// use base class documentation
void AbstractFixedSizeSubsetGenerator::fromVariant(const QVariant& variant)
{
    AbstractSubsetGenerator::fromVariant(variant);

    QVariantMap varMap = variant.toMap();

    setNbSubsets(varMap.value(QStringLiteral("number of subsets"), 1u).toUInt());
}

// use base class documentation
QVariant AbstractFixedSizeSubsetGenerator::toVariant() const
{
    QVariantMap ret = AbstractSubsetGenerator::toVariant().toMap();

    ret.insert(QStringLiteral("number of subsets"), _nbSubsets);

    return ret;
}


namespace assist {
/*!
 * \brief Convenience helper function that calculates sizes for subsets given the total number of
 * projections \a nbViews, the requested number of subsets \a nbSubsets and an arrangement type.
 *
 * This method returns a vector containing the number of projections each subset should contain to
 * partition the total number of projections (\a nbViews) into the requested number of subsets
 * (\a nbSubsets).
 *
 * Returns an empty vector for \a nbSubsets = 0. If \a nbSubsets > \a nbViews, returns a vector
 * containing \a nbViews ones.
 *
 * When \f$nbViews\;(\textrm{mod}\;nbSubsets)=0\f$, the partitioning is trivial and each subset contains
 * \f$ nbViews / nbSubsets\f$ projections.
 * The SubsetArrangement \a arrangement provides three different options how to proceed when the
 * total number of projections (i.e. \a nbViews) is not divisable by the requested number of subsets
 * \a nbSubsets. In particular, the choices are:
 * - SubsetArrangement::Evenly: try to minimize differences in size between all generated subsets.
 * - SubsetArrangement::LastIsLargest: create \a nbSubsets - 1 equally-sized subsets such that the
 * last subset is larger than the others when taking all the remaining projections.
 * - SubsetArrangement::LastIsSmallest: create \a nbSubsets - 1 equally-sized subsets such that the
 * last subset is smaller than the others when taking all the remaining projections.
 *
 * Example:
 * \code
 *  qInfo() << assist::subsetSizes(23, 5, assist::SubsetArrangement::Evenly);
 *  qInfo() << assist::subsetSizes(23, 5, assist::SubsetArrangement::LastIsLargest);
 *  qInfo() << assist::subsetSizes(23, 5, assist::SubsetArrangement::LastIsSmallest);
 *
 *      // output:
 *      // std::vector(4, 4, 5, 5, 5)
 *      // std::vector(4, 4, 4, 4, 7)
 *      // std::vector(5, 5, 5, 5, 3)
 * \endcode
 *
 * \relates CTL::AbstractFixedSizeSubsetGenerator
 */
std::vector<uint> subsetSizes(uint nbViews, uint nbSubsets, SubsetArrangement arrangement)
{
    std::vector<uint> subsetSizes;

    if(nbSubsets == 0)
        return subsetSizes;

    if(nbSubsets > nbViews)
        return std::vector<uint>(nbViews, 1u);

    switch (arrangement) {
    case SubsetArrangement::Evenly:
    {
        uint sizeSmallSubsets = std::max(nbViews / nbSubsets, 1u);
        uint nbLargeSubsets   = nbViews - nbSubsets * sizeSmallSubsets;

        subsetSizes = std::vector<uint>(nbSubsets, sizeSmallSubsets);

        auto backIt = subsetSizes.rbegin();
        for(uint s = 0; s < nbLargeSubsets; ++s)
            *(backIt++) += 1u;

        break;
    }
    case SubsetArrangement::LastIsLargest:
    {
        uint sizeSmallSubsets = std::max(nbViews / nbSubsets, 1u);
        int sizeLastSubset    = int(nbViews) - int((nbSubsets-1u) * sizeSmallSubsets);

        subsetSizes = std::vector<uint>(nbSubsets, sizeSmallSubsets);
        subsetSizes.back() = sizeLastSubset;

        break;
    }
    case SubsetArrangement::LastIsSmallest:
    {
        uint sizeLargeSubsets = uint(std::ceil(float(nbViews) /float(nbSubsets)));
        int sizeLastSubset    = int(nbViews) - int(nbSubsets - 1u) * sizeLargeSubsets;

        subsetSizes = std::vector<uint>(nbSubsets, sizeLargeSubsets);
        subsetSizes.back() = sizeLastSubset;

        break;
    }
    }

    return subsetSizes;
}

} // namespace assist

} // namespace CTL
