#ifndef CTL_ORTHOGONALSUBSETGENERATOR_H
#define CTL_ORTHOGONALSUBSETGENERATOR_H

#include "abstractsubsetgenerator.h"
#include "mat/matrix.h"

namespace CTL {

/*!
 * \brief The OrthogonalSubsetGenerator class provides an implementation of subset generation
 * considering real geometric orthogonality.
 *
 * This class implements the AbstractFixedSizeSubsetGenerator interface. The number of subsets that
 * shall be generated can be specified in the constructor or later on through setNbSubsets().
 *
 * The subset generation routine implemented by this class depends on the acquisition geometry
 * referring to the projections that shall be divided into subsets. For that reason, the
 * corresponding AcquisitionSetup needs to be set in advance of generating the subsets. This can be
 * done individually through setSetup() or along with setting projection data using setData().
 *
 * In order to assign individual views to a particular subset, the degree of orthogonality between
 * the view directions (i.e. the source-to-detector vector) of all views already included in the
 * subset and all remaining candidates (i.e. views from the full projection data that are not yet
 * assigned to any subset) is computed. The candidate maximizing the average orthogonality is then
 * subsequnetly added to the subset under construction and the full procedure is repeated until the
 * required number (subject to the requested number of subsets) of views is reached.
 *
 * Example: creating two subsets for a set of 18 views from a ShortScanTrajectory
 * \code
 *  constexpr uint nbViews = 18;
 *
 *  // first, we create the AcquisitionSetup with 'nbViews' and a ShortScanTrajectory
 *  AcquisitionSetup setup(makeSimpleCTSystem<blueprints::GenericCarmCT>());
 *  setup.setNbViews(nbViews);
 *  setup.applyPreparationProtocol(protocols::ShortScanTrajectory(700.0));
 *
 *  // create some dummy projections (the actual dimensions are irrelevant here)
 *  ProjectionData dummyProjections(1,1,1);
 *  dummyProjections.allocateMemory(nbViews); // dummyProjections now contains 'nbViews' views
 *
 *  // create our subset generator and set the number of subsets to 2
 *  OrthogonalSubsetGenerator subsetGen;
 *  subsetGen.setNbSubsets(2);
 *
 *  // pass both projection data and the correponding setup to the generator
 *  subsetGen.setData(dummyProjections, setup);
 *
 *  // create the subsets (note: iteration '0' is entirely arbitrary and has no particular effect)
 *  const auto subsets = subsetGen.generateSubsets(0);
 *
 *  // print the ids of the views that have been assigned to our two subsets
 *  for(const auto& subset : subsets)
 *      qInfo() << subset.viewIds();
 *
 *      // output:
 *      // std::vector(17, 10, 5, 13, 3, 11, 2, 16, 1)
 *      // std::vector(0, 8, 4, 12, 6, 14, 7, 15, 9)
 *
 *
 *  // out of interest:
 *  // we want to compare that to a SimpleSubsetGenerator that assumes a 180 degree circle
 *  SimpleSubsetGenerator simpleGen(2, SimpleSubsetGenerator::Orthogonal180);
 *  simpleGen.setProjections(dummyProjections); // note that we do not need the AcquisitionSetup here
 *
 *  for(const auto& subset : simpleGen.generateSubsets(0))
 *      qInfo() << subset.viewIds();
 *
 *      // output:
 *      // std::vector(12, 1, 10, 8, 17, 5, 14, 6, 15)
 *      // std::vector(0, 9, 4, 13, 7, 16, 2, 11, 3)
 * \endcode
 */
class OrthogonalSubsetGenerator : public AbstractFixedSizeSubsetGenerator
{
    CTL_TYPE_ID(1011)

    protected: std::vector<ProjectionDataView> generateSubsetsImpl(uint iteration) const override;

public:
    OrthogonalSubsetGenerator(uint nbSubsets = 1u);
    // AbstractSubsetGenerator interface
    void setSetup(const AcquisitionSetup& setup) override;

private:
    struct Candidate
    {
        mat::Matrix<3,1> direction;
        uint viewID;
    };

    std::vector<mat::Matrix<3,1>> _directionVectors; //!< source-to-detector vectors of the latest setup set

    ProjectionDataView createSubset(std::vector<Candidate>& candidates, uint subsetSize) const;
};

} // namespace CTL

#endif // CTL_ORTHOGONALSUBSETGENERATOR_H
