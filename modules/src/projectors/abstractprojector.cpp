#include "abstractprojector.h"

#include "img/compositevolume.h"
#include "img/sparsevoxelvolume.h"
#include "io/messagehandler.h"

namespace CTL {

/*!
 * \fn void AbstractProjector::configure(const AcquisitionSetup& setup)
 *
 * \brief Configures the projector.
 *
 * This method should be used to gather all necessary information to prepare the actual forward
 * projection. This usually contains all geometry and system information, which can be retrieved
 * from \a setup.
 *
 * If you intend to call configure() and project() (or projectComposite()) directly after
 * each other, you should use configureAndProject() instead.
 */

/*!
 * \fn ProjectionData AbstractProjector::project(const VolumeData& volume)
 *
 * \brief Provides the actual forward projection functionality.
 *
 * This method takes a voxelized dataset \a volume and shall return the full set of forward
 * projections that have been requested by the AcquisitionSetup set in the configure() step.
 *
 * The passed volume data can be either:
 * - SpectralVolumeData,
 * - VoxelVolume<float> (implicitely converted to SpectralVolumeData),
 * - any sub-class of AbstractDynamicVolumeData.
 *
 * CompositeVolume data can be projected using projectComposite(); SparseVoxelVolume input can be
 * processed by projectSparse().
 *
 * Note that the functionality of specific ProjectorExtension classes might depend on a passing a
 * certain type of volume data. Please refer to the documentation of the extensions you are using.
 *
 * If you intend to call configure() and project() directly after each other, you should
 * use configureAndProject() instead.
 */

/*!
 * \brief Returns true if the projection operation is linear.
 *
 * By default, this method returns true. Override this method to return false in case of
 * sub-classing that leads to non-linear operations. Overrides of this method should never return
 * an unconditional \c true (as this might outrule underlying non-linearity).
 */
bool AbstractProjector::isLinear() const { return true; }

/*!
 * \brief Provides the functionality to forward project CompositeVolume data.
 *
 * This method takes a composite dataset \a volume and returns the full set of forward projections
 * according to the AcquisitionSetup set in the configure() step.
 *
 * By default, this method performs separate calls to project() for each individual voxel volume
 * stored in the composite \a volume. The final projection result is the sum of all these individual
 * projections (extinction domain).
 * Change this behavior in sub-classes, if this is not suitable for your desired purpose. This is
 * typically the case for non-linear operations.
 *
 * If you intend to call configure() and projectComposite() directly after each other, you should
 * use configureAndProject() instead.
 */
ProjectionData AbstractProjector::projectComposite(const CompositeVolume& volume)
{
    if(volume.isEmpty())
        throw std::runtime_error("AbstractProjector::projectComposite: Volume is empty.");

    // project first sub volume
    ProjectionData ret = project(volume.subVolume(0));

    // project remaining sub volumes
    for(auto subVol = 1u, nbSubVol = volume.nbSubVolumes(); subVol < nbSubVol; ++subVol)
        ret += project(volume.subVolume(subVol));

    return ret;
}

/*!
* \brief Provides the functionality to forward project SparseVoxelVolume data.
*
* This method takes a sparse dataset \a volume and returns the full set of forward projections
* according to the AcquisitionSetup set in the configure() step.
*
* By default, this method performs converts \a volume to a regular VoxelVolume<float> and calls
* project() with it. Re-implement this method in sub-classes if you can provide more efficient ways
* of forward projecting sparse data.
*
* If you intend to call configure() and projectSparse() directly after each other, you should
* use configureAndProject() instead.
*/
ProjectionData AbstractProjector::projectSparse(const SparseVoxelVolume& volume)
{
    if(volume.nbVoxels() == 0)
        throw std::runtime_error("AbstractProjector::projectSparse: Volume is empty.");

    qWarning("Using default implementation of AbstractProjector::projectSparse(). Sparse volume is"
             " converted to regular VoxelVolume before projecting.");

    return project(volume.toVoxelVolume());
}

/*!
 * \brief Performs a forward projection with a precedent configuration of the projector.
 *
 * This convenience method combines the calls to configure() and project() into a single call.
 * Returns the result of `project`.
 *
 * Same as:
 * \code
 * configure(setup);
 * return project(volume);
 * \endcode
 *
 * Using this method is recommended over individual calls of configure() and project() directly
 * after one another, because it may help you avoid mistakes. In particular, it prevents that
 * configure() is missed to call before calling project(), e.g. after changing settings of the
 * projector (which usually require a re-`configure`).
 *
 * Note that this method usually changes the state of the projector due to the `configure` step.
 *
 * \sa configure, project
 */
ProjectionData AbstractProjector::configureAndProject(const AcquisitionSetup& setup,
                                                      const VolumeData& volume)
{
    this->configure(setup);
    return this->project(volume);
}

/*!
 * \brief Performs a forward projection with a precedent configuration of the projector.
 *
 * This convenience method combines the calls to configure() and projectComposite() into a single
 * call. Returns the result of `projectComposite`.
 *
 * Same as:
 * \code
 * configure(setup);
 * return projectComposite(volume);
 * \endcode
 *
 * Using this method is recommended over individual calls of configure() and projectComposite()
 * directly after one another, because it may help you avoid mistakes. In particular, it prevents
 * that configure() is missed to call before calling project(), e.g. after changing settings of the
 * projector (which usually require a re-`configure`).
 *
 * Note that this methods usually change the state of the projector due to the `configure` step.
 *
 * \sa configure, projectComposite
 */
ProjectionData AbstractProjector::configureAndProject(const AcquisitionSetup& setup,
                                                      const CompositeVolume& volume)
{
    this->configure(setup);
    return this->projectComposite(volume);
}

/*!
 * \brief Performs a forward projection with a precedent configuration of the projector.
 *
 * This convenience method combines the calls to configure() and projectSparse() into a single
 * call. Returns the result of `projectSparse`.
 *
 * Same as:
 * \code
 * configure(setup);
 * return projectSparse(volume);
 * \endcode
 *
 * Using this method is recommended over individual calls of configure() and projectSparse()
 * directly after one another, because it may help you avoid mistakes. In particular, it prevents
 * that configure() is missed to call before calling project(), e.g. after changing settings of the
 * projector (which usually require a re-`configure`).
 *
 * Note that this methods usually change the state of the projector due to the `configure` step.
 *
 * \sa configure, projectSparse
 */
ProjectionData AbstractProjector::configureAndProject(const AcquisitionSetup& setup,
                                                      const SparseVoxelVolume& volume)
{
    this->configure(setup);
    return this->projectSparse(volume);
}

/*!
 * \brief Returns the parameters of this instance as QVariant.
 *
 * This shall return a QVariantMap with key-value-pairs representing all settings of the object.
 *
 * This method is used within toVariant() to serialize the object's settings.
 */
QVariant AbstractProjector::parameter() const { return QVariant(); }

/*!
 * \brief Sets the parameters of this instance based on the passed QVariant \a parameter.
 * Parameters need to follow the naming convention as described in parameter().
 *
 * This method is used within fromVariant() to deserialize the object's settings. Direct use of
 * this method is discouraged; consider using dedicated setter methods instead.
 */
void AbstractProjector::setParameter(const QVariant&) {}

/*!
 * \brief Stores the contents of this instance in a QVariant.
 *
 * Implementation of the serialization interface. Stores the object's type-id (from
 * SerializationInterface::toVariant()).
 *
 * This method uses parameter() to serialize class members.
 */
QVariant AbstractProjector::toVariant() const
{
    QVariantMap ret = SerializationInterface::toVariant().toMap();

    ret.insert(QStringLiteral("parameters"), parameter());

    return ret;
}

/*!
 * \brief Sets the contents of the object based on the QVariant \a variant.
 *
 * Implementation of the deserialization interface.
 * This method uses setParameter() to deserialize class members.
 */
void AbstractProjector::fromVariant(const QVariant& variant)
{
    SerializationInterface::fromVariant(variant);

    const auto map = variant.toMap();

    setParameter(map.value(QStringLiteral("parameters")).toMap());
}

/// \brief Returns a pointer to the notifier of the projector.
///
/// The notifier object can be used to emit the signal ProjectorNotifier::projectionFinished(int)
/// when the calculation of the \a viewNb'th view has been done.
///
/// To receive emitted signals, use Qt's connect() method to connect the notifier object to any
/// receiver object of choice. The notifier can also conveniently be connected to the MessageHandler
/// of the CTL using connectToMessageHandler() or directly from the projector through
/// AbstractProjector::connectNotifierToMessageHandler().
///
/// Example - sending simulation progress information to a QProgressBar:
/// \code
/// AbstractProjector* myProjector /* = new ActualProjector */;
///
/// QProgressBar* myProgressBar = new QProgressBar();
/// // ... e.g. add the progress bar somewhere in your GUI
///
/// connect(myProjector->notifier(), SIGNAL(projectionFinished(int)),
///         myProgressBar, SLOT(setValue(int)));
/// \endcode
///
/// /// Example 2 - forwarding the information text to a QStatusBar
/// \code
/// AbstractProjector* myProjector /* = new ActualProjector */;
///
/// QStatusBar* myStatusBar = new QStatusBar;
/// // ... e.g. add the status bar somewhere in your GUI (or take a pointer to an existing one)
///
/// QObject::connect(myProjector->notifier(), SIGNAL(information(QString)),
///                  myStatusBar, SLOT(showMessage(QString)));
/// \endcode
ProjectorNotifier* AbstractProjector::notifier() { return _notifier.get(); }

/*!
 * \brief Connects the notifier to the CTL's MessageHandler.
 *
 * Same as:
 * \code
 * notifier()->connectToMessageHandler(includeProgress);
 * \endcode
 *
 * \sa ProjectorNotifier::connectToMessageHandler
 */
void AbstractProjector::connectNotifierToMessageHandler(bool includeProgress)
{
    notifier()->connectToMessageHandler(includeProgress);
}

/*!
 * \brief Connects this instance's signals to the CTL's MessageHandler.
 *
 * This connects the information signal of this instance to the MessageHandler::messageFromSignal
 * slot, such that it processes incoming information signals as regular messages.
 * If \a includeProgress = \c true, this also establishes a connection between this instance's
 * projectionFinished() signal and the message handler. Each `projectionFinished` signal results in
 * a message of format "%1 projections finished.", where %1 is replaced with the number of finished
 * projections.
 *
 * The method ensures that no multiple connections are established, even if it is called multiple
 * times.
 *
 * To (fully) disconnect this instance from the message handler, use:
 * \code
 * this->disconnect(&MessageHandler::instance());
 * \endcode
 */
void ProjectorNotifier::connectToMessageHandler(bool includeProgress)
{
    QObject::connect(this, &ProjectorNotifier::information,
                    &MessageHandler::instance(), &MessageHandler::messageFromSignal,
                    Qt::UniqueConnection);

    if(includeProgress)
    {
       const auto progressString = [] (int prog)
       {
           MessageHandler::instance().messageFromSignal(
                       QStringLiteral("%1 projections finished.").arg(prog));
       };
       QObject::disconnect(_progressConnection); // avoid multiple connections
       _progressConnection = QObject::connect(this, &ProjectorNotifier::projectionFinished,
                                              &MessageHandler::instance(), progressString);
    }
}

/*!
 * \fn void ProjectorNotifier::projectionFinished(int viewNb)
 *
 * Signal that is emitted after processing of projection \a viewNb is finished.
 */

/*!
 * \fn void ProjectorNotifier::information(QString info)
 *
 * Signal that can be emitted to communicate a status information.
 */

/*!
 * \fn AbstractProjector::~AbstractProjector()
 *
 * Virtual default destructor.
 */

} // namespace CTL
