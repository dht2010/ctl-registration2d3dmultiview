#ifndef CTL_QTCHARTSNAMESPACE_H
#define CTL_QTCHARTSNAMESPACE_H

//!
//! This empty definition of the `QtCharts` namespace provides compatibility with Qt5 and Qt6.
//!
inline namespace QtCharts {

} // namespace QtCharts

#endif // CTL_QTCHARTSNAMESPACE_H
