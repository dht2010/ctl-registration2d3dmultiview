#include "datamodelviewer.h"
#include "ui_datamodelviewer.h"

#include "processing/coordinates.h"
#include "lineseriesview.h"
#include "intervalseriesview.h"

#include <qmath.h>

namespace CTL {
namespace gui {

/*!
 * Creates a DataModelViewer and sets its parent to \a parent.
 */
DataModelViewer::DataModelViewer(QWidget *parent)
    : QWidget(parent)
    , _lineView(new LineSeriesView)
    , _intervalView(new IntervalSeriesView)
    , ui(new Ui::DataModelViewer)
{
    ui->setupUi(this);

    _lineView->setShowPoints();
    ui->_stackedWidget->addWidget(_lineView);
    ui->_stackedWidget->addWidget(_intervalView);
    ui->_stackedWidget->setCurrentWidget(_lineView);

    connect(ui->_RB_values, &QRadioButton::toggled, this, &DataModelViewer::updatePlot);
    connect(ui->_SB_rangeFrom, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DataModelViewer::updatePlot);
    connect(ui->_SB_rangeTo, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &DataModelViewer::updatePlot);
    connect(ui->_PB_reduceSampling, &QPushButton::clicked, this, &DataModelViewer::reduceSamplingDensity);
    connect(ui->_PB_increaseSampling, &QPushButton::clicked, this, &DataModelViewer::increaseSamplingDensity);
    connect(ui->_SB_nbSamples, QOverload<int>::of(&QSpinBox::valueChanged), this, &DataModelViewer::updatePlot);
    connect(ui->_W_parameterEditor, &ParameterConfigWidget::parameterChanged, this, &DataModelViewer::setModelParameter);
    connect(ui->_PB_linLogY, &QPushButton::clicked, this, &DataModelViewer::toggleLogY);
    connect(ui->_CB_niceX, &QCheckBox::toggled, _lineView, &LineSeriesView::setUseNiceX);
    connect(ui->_CB_niceX, &QCheckBox::toggled, _intervalView, &IntervalSeriesView::setUseNiceX);

    setWindowTitle("Data Model Viewer");
}

/*!
 * Deletes this instance.
 */
DataModelViewer::~DataModelViewer()
{
    delete ui;
}

/*!
 * \brief Creates a DataModelViewer for \a model and shows the window.
 *
 * Note that this instance takes a copy of \a model through its clone() method.
 * \a model must not be nullptr; throws an std::runtime_error otherwise.
 *
 * Labels of the axes can be specified by \a labelX and \a labelY. If left empty, default axis
 * labels are "x" and "y".
 *
 * Depending on whether or not \a model is integrable (see AbstractIntegrableDataModel), the widget
 * also offers the option to visualize bin integrals.
 *
 * The widget will be deleted automatically if the window is closed.
 *
 * Example:
 * \code
 * // get the attenuation model of lead from the database
 * auto attModel = attenuationModel(database::Element::Pb);
 *
 * // visualize
 * gui::DataModelViewer::plot(attModel, "Energy [keV]", "Mass atten. coeff. [cm^2/g]");
 * \endcode
 */
void DataModelViewer::plot(std::shared_ptr<AbstractDataModel> model,
                           const QString& labelX, const QString& labelY)
{
    if(!model)
        throw std::runtime_error("DataModelViewer::plot: model must not be nullptr.");

    plot(*model, labelX, labelY);
}

/*!
 * \brief Creates a DataModelViewer for \a model and shows the window.
 *
 * Note that this instance takes a copy of \a model through its clone() method.
 *
 * Labels of the axes can be specified by \a labelX and \a labelY. If left empty, default axis
 * labels are "x" and "y".
 *
 * Depending on whether or not \a model is integrable (see AbstractIntegrableDataModel), the widget
 * also offers the option to visualize bin integrals.
 *
 * The widget will be deleted automatically if the window is closed.
 *
 * Example:
 * \code
 * // create a one-dimensional Gaussian model
 * auto model = GaussianModel1D();
 *
 * // visualize
 * gui::DataModelViewer::plot(model);
 * \endcode
 */
void DataModelViewer::plot(const AbstractDataModel& model, const QString& labelX,
                           const QString& labelY)
{

    auto viewer = new DataModelViewer;
    viewer->setAttribute(Qt::WA_DeleteOnClose);

    viewer->setData(model);

    viewer->setLabelX(labelX);
    viewer->setLabelY(labelY);

    viewer->resize(800, 600);
    viewer->show();
}

/*!
 * Returns the viewport for displaying the line series data in this instance. Use this to adjust its
 * specific settings if required.
 *
 * Example: setting a specific axis range for the line series view and changing the chart title
 * \code
 * auto viewer = new gui::DataModelViewer;
 * // ...
 *
 * viewer->dataViewValues()->setRangeX(20.0, 30.0);
 * viewer->dataViewValues()->chart()->setTitle("Example title");
 * viewer->show();
 * \endcode
 *
 * \sa LineSeriesView.
 */
LineSeriesView* DataModelViewer::dataViewValues() const
{
    return _lineView;
}

/*!
 * Returns the viewport for displaying the bin integral data in this instance. Use this to adjust
 * its specific settings if required.
 *
 * Example: setting a dark theme for the bin integral chart
 * \code
 * auto viewer = new gui::DataModelViewer;
 * // ...
 *
 * viewer->dataViewBinIntegrals()->chart()->setTheme(QtCharts::QChart::ChartThemeDark);
 * viewer->show();
 * \endcode
 *
 * \sa IntervalSeriesView.
 */
IntervalSeriesView* DataModelViewer::dataViewBinIntegrals() const
{
    return _intervalView;
}

/*!
 * \brief Sets the data model visualized by this instance to \a model.
 *
 * Note that this will clone the model, such that the viewer instance will own a copy of \a model.
 * \a model must not be nullptr; throws an std::runtime_error otherwise.
 */
void DataModelViewer::setData(std::shared_ptr<AbstractDataModel> model)
{
    if(!model)
        throw std::runtime_error("DataModelViewer::setData: model must not be nullptr.");

    setData(*model);
}

/*!
 * \brief Sets the data model visualized by this instance to \a model.
 *
 * Note that this will clone the model, such that the viewer instance will own a copy of \a model.
 */
void DataModelViewer::setData(const AbstractDataModel& model)
{
    _model.reset(model.clone());

    if(_model->isIntegrable())
        ui->_RB_binIntegrals->setEnabled(true);
    else
    {
        ui->_RB_binIntegrals->setEnabled(false);
        ui->_RB_values->setChecked(true);
    }

    _lineView->chart()->setTitle(_model->name());
    _intervalView->chart()->setTitle(_model->name());

    ui->_W_parameterEditor->updateInterface(_model->parameter());
    ui->_W_parameterEditor->isEmpty() ? ui->_GB_parameter->hide() : ui->_GB_parameter->show();

    updatePlot();
}

// public slots

/*!
 * Increases the number of sampling points by 25% of their current value.
 */
void DataModelViewer::increaseSamplingDensity()
{
    setNumberOfSamples(qCeil(ui->_SB_nbSamples->value() * 1.25));
}

/*!
 * Hides the model parameter GUI element if \a hide = \c true and shows it otherwise.
 */
void DataModelViewer::hideParameterGUI(bool hide)
{
    hide ? ui->_GB_parameter->hide() : ui->_GB_parameter->show();
}

/*!
 * Reduces the number of sampling points to 80% of their current value.
 */
void DataModelViewer::reduceSamplingDensity()
{
    setNumberOfSamples(qCeil(ui->_SB_nbSamples->value() * 0.8));
}

/*!
 * Sets the label of the *x*-axis of both plot types to \a label.
 */
void DataModelViewer::setLabelX(const QString& label)
{
    _lineView->setLabelX(label);
    _intervalView->setLabelX(label);
}

/*!
 * Sets the label of the *y*-axis of both plot types to \a label.
 */
void DataModelViewer::setLabelY(const QString& label)
{
    _lineView->setLabelY(label);
    _intervalView->setLabelY(label);
}

/*!
 * Sets the number of sampling points to \a nbSamples.
 */
void DataModelViewer::setNumberOfSamples(int nbSamples)
{
    ui->_SB_nbSamples->setValue(nbSamples);
}

/*!
 * Sets the range within which the model is sampled to [\a from, \a to].
 */
void DataModelViewer::setSamplingRange(float from, float to)
{
    ui->_SB_rangeFrom->setValue(from);
    ui->_SB_rangeTo->setValue(to);
}

/*!
 * Toggles between linear and logarithmic *y*-axis display.
 */
void DataModelViewer::toggleLogY()
{
    _lineView->toggleLinLogY();
    _intervalView->toggleLinLogY();

    if(QObject::sender() != ui->_PB_linLogY)
    {
        ui->_PB_linLogY->blockSignals(true);
        ui->_PB_linLogY->toggle();
        ui->_PB_linLogY->blockSignals(false);
    }
}

/*!
 * Updates the current plot. This will readout all UI elements for information on sampling and
 * performs a new sampling of the values from the model.
 *
 * This is called automatically when necessary.
 */
void DataModelViewer::updatePlot()
{
    const auto nbSamples = ui->_SB_nbSamples->value();
    const auto range = SamplingRange(ui->_SB_rangeFrom->value(), ui->_SB_rangeTo->value());

    if(ui->_RB_values->isChecked())
    {
        auto sampledValues = XYDataSeries::sampledFromModel(*_model, range.linspace(nbSamples));

        _lineView->setData(sampledValues.data());
        ui->_stackedWidget->setCurrentWidget(_lineView);
    }
    else // bin integrals is checked
    {
        auto sampledValues = IntervalDataSeries::sampledFromModel(
                    static_cast<const AbstractIntegrableDataModel&>(*_model),
                    range.start(), range.end(), nbSamples);

        _intervalView->setData(sampledValues);
        ui->_stackedWidget->setCurrentWidget(_intervalView);
    }
}

void DataModelViewer::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::CTRL && event->key() == Qt::Key_S)
    {
        if(ui->_stackedWidget->currentWidget() == _lineView)
            _lineView->saveDialog();
        else
            _intervalView->saveDialog();

        event->accept();
    }

    QWidget::keyPressEvent(event);
}

void DataModelViewer::setModelParameter(QVariant parameter)
{
    _model->setParameter(parameter);
    updatePlot();
}


} // namespace gui
} // namespace CTL
