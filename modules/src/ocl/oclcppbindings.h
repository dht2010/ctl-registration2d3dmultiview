#ifndef CTL_OCLCPPBINDINGS_H
#define CTL_OCLCPPBINDINGS_H

#define __CL_ENABLE_EXCEPTIONS
#define CL_TARGET_OPENCL_VERSION 120
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#if defined(__APPLE__) || defined(__MACOSX)
#   include <OpenCL/cl.hpp>
#else // not Apple
#   if defined __has_include
#      if __has_include(<CL/cl2.hpp>) || __has_include(<CL/opencl.hpp>)
#          define CL_HPP_ENABLE_EXCEPTIONS
#          define CL_HPP_TARGET_OPENCL_VERSION 120
#          define CL_HPP_MINIMUM_OPENCL_VERSION 110
#          define CL_HPP_CL_1_2_DEFAULT_BUILD
#          define CL_HPP_ENABLE_SIZE_T_COMPATIBILITY
#          define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY
#          if __has_include(<CL/opencl.hpp>)
#              include <CL/opencl.hpp>
#          else // old OpenCL 2.x header
#              include <CL/cl2.hpp>
#          endif
#      else // old OpenCL 1.x header
#          include <CL/cl.hpp>
#      endif
#   else // old OpenCL 1.x header
#       include <CL/cl.hpp>
#   endif
#endif

// OpenCL version < 1.2 compatibility
#ifndef CL_VERSION_1_2
#   define CL_MEM_HOST_WRITE_ONLY 0
#   define CL_MEM_HOST_READ_ONLY 0
#endif

#endif // CTL_OCLCPPBINDINGS_H
