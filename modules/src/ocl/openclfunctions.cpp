#include "openclfunctions.h"
#include <algorithm>

namespace CTL {

// +++++++++++++ OpenCLFunctions class implementation +++++++++++++

/*!
 * Constructs a OpenCLFunctions instance from an `std::initializer_list` \a functionList.
 * An example use case is given by
 * \code
 *   OpenCLFunctions myFunctions = { OpenCLFunctions::write_bufferf, OpenCLFunctions::atomic_addf_l };
 * \endcode
 *
 * As this constructor is implicit, the right-hand side of the aboves code can be also directly
 * passed to the `OpenCLConfig::addKernel` function.
 *
 * The input type `std::initializer_list` has `OpenCLFunctionGen` as template argument. This helper
 * class defines a function type (generator function) with the signature of the `static`
 * functions provided by the OpenCLFunctions class, which in turn, when invoked, return the actual
 * `OpenCLFunction` object. The invocation of the generator functions in \a functionList is
 * performed in this constructor and the returned `OpenCLFunction` objects are stored internally.
 */
OpenCLFunctions::OpenCLFunctions(std::initializer_list<OpenCLFunctionGen> functionList)
{
    _functions.reserve(functionList.size());

    for(const auto& f : functionList)
        _functions.push_back(f());
}

/*!
 * Constructs a OpenCLFunctions instance that contains a single \a function.
 * An example use case is given by
 * \code
 *   OpenCLFunctions myFunctions = OpenCLFunctions::write_bufferf;
 * \endcode
 *
 * As this constructor is implicit, the right-hand side of the aboves code can be also directly
 * passed to the `OpenCLConfig::addKernel` function.
 *
 * The input type `OpenCLFunctionGen` defines a function type (generator function) with the
 * signature of the `static` functions provided by the OpenCLFunctions class, which in turn, when
 * invoked, return the actual `OpenCLFunction` object. The invocation of the generator \a function
 * is performed in this constructor and the returned `OpenCLFunction` object is stored internally.
 *
 * An instance of OpenCLFunctions may be also constructed from a custom function using a lambda
 * expression:
 * \code
 *   const auto myFun = OpenCLFunctions([]() -> OpenCLFunctions::OpenCLFunction
 *   {
 *       return
 *   R"clSrc(void myFun(){
 *
 *   // do something
 *
 *   }
 *   )clSrc";
 *   });
 * \endcode
 */
OpenCLFunctions::OpenCLFunctions(OpenCLFunctionGen function)
    : OpenCLFunctions{ function }
{
}

/*!
 * Returns the list of the internally stored `OpenCLFunction`s.
 */
const std::vector<OpenCLFunctions::OpenCLFunction>& OpenCLFunctions::functions() const
{
    return _functions;
}

/*!
 * Returns all declarations of the internally handled `OpenCLFunction`s as a single string.
 */
std::string OpenCLFunctions::declarations() const
{
    std::string ret;

    for(const auto& fun : _functions)
        ret.append(fun.declaration());

    return ret;
}

/*!
 * Returns all declarations of the internally handled `OpenCLFunction`s followed by all definitions
 * of these functions as a single string.
 */
std::string OpenCLFunctions::declarationsAndDefinitions() const
{
    std::string ret = declarations();

    for(const auto& fun : _functions)
        ret.append(fun.definition());

    return ret;
}

// #### Source code of OpenCL C functions ####

/*!
 * `inline void atomic_addf_g(volatile global float* addr, float val)`
 *
 * This function is supposed to extend the capabilities of the built-in function `atomic_add`.
 * The built-in function works only with integer types, whereas this function works with `float`.
 * In an atomic manner it does the following:
 * It reads the 32-bit value floating point value stored at location pointed by `addr`. It computes
 * the sum with `val` and stores back the result to the location pointed by `addr`.
 *
 * Internally, the implementation uses `atomic_cmpxchg` in a loop. Depending on the simultaneous
 * write accesses to the location `addr` (by other workers), the function may take several
 * iterations to terminate.
 *
 * The `_g` in the function name indicates that it can only be used with an `addr` pointing to
 * `global` qualified memory. For `local` memory, see `atomic_addf_l`.
 *
 * Note that this function does not return anything in contrast to the OpenCL built-in function.
 */
OpenCLFunctions::OpenCLFunction OpenCLFunctions::atomic_addf_g()
{
    return
R"clSrc(inline void atomic_addf_g(volatile global float* addr, float val){
    union {
        unsigned int u32;
        float f32;
    } next, expected, current;

    current.f32 = *addr;
    do {
        expected.f32 = current.f32;
        next.f32 = expected.f32 + val;
        current.u32 = atomic_cmpxchg((volatile global unsigned int*)addr, expected.u32, next.u32);
    } while(current.u32 != expected.u32);
}
)clSrc";
}

/*!
 * `inline void atomic_addf_l(volatile local float* addr, float val)`
 *
 * This function is supposed to extend the capabilities of the built-in function `atomic_add`.
 * The built-in function works only with integer types, whereas this function works with `float`.
 * In an atomic manner it does the following:
 * It reads the 32-bit value floating point value stored at location pointed by `addr`. It computes
 * the sum with `val` and stores back the result to the location pointed by `addr`.
 *
 * Internally, the implementation uses `atomic_cmpxchg` in a loop. Depending on the simultaneous
 * write accesses to the location `addr` (by other workers), the function may take several
 * iterations to terminate.
 *
 * The `_l` in the function name indicates that it can only be used with an `addr` pointing to
 * `local` qualified memory. For `global` memory, see `atomic_addf_g`.
 *
 * Note that this function does not return anything in contrast to the OpenCL built-in function.
 */
OpenCLFunctions::OpenCLFunction OpenCLFunctions::atomic_addf_l()
{
    return
R"clSrc(inline void atomic_addf_l(volatile local float* addr, float val){
    union {
        unsigned int u32;
        float f32;
    } next, expected, current;

    current.f32 = *addr;
    do {
        expected.f32 = current.f32;
        next.f32 = expected.f32 + val;
        current.u32 = atomic_cmpxchg((volatile local unsigned int*)addr, expected.u32, next.u32);
    } while(current.u32 != expected.u32);
}
)clSrc";
}

/*!
 * `void write_bufferf(global float* buffer, int4 coord, float color, image3d_t image)`
 * 
 * This function imitates the behavior of the function `write_imagef` for buffers of
 * vectorized 3d float images instead of `image3d_t`. Unlike `write_imagef`, the extension
 * `cl_khr_3d_image_writes` does not need to be implemented for `write_bufferf`.
 * 
 * Note that no bounds checking is performed on `coord`.
 * 
 * In contrast to `write_imagef`, the `color` argument is a single `float` value.
 * 
 * The extents of the 3d image are inherited from the last argument `image`.
 */
OpenCLFunctions::OpenCLFunction OpenCLFunctions::write_bufferf()
{
    return
R"clSrc(void write_bufferf(global float* buffer, int4 coord, float color, image3d_t image){
    const int width = get_image_width(image);
    const int height = get_image_height(image);

    buffer[coord.x + coord.y * width + coord.z * width * height] = color;
}
)clSrc";
}

// +++++++++++++ OpenCLFunction class implementation +++++++++++++

/*!
 * Constructs an instance of OpenCLFunction from \a clSourceCode that is supposed to be the
 * OpenCL C source code (defintion) of an OpenCL function.
 *
 * The source code should be formatted as
 * \code
 *   <function signature>{
 *   // implementation of the OpenCL function
 *   }
 * \endcode
 */
OpenCLFunctions::OpenCLFunction::OpenCLFunction(const char* clSourceCode)
    : _code(clSourceCode)
{
}

/*!
 * Constructs an instance of OpenCLFunction from \a clSourceCode that is supposed to be the
 * OpenCL C source code (defintion) of an OpenCL function.
 *
 * The source code should be formatted as
 * \code
 *   <function signature>{
 *   // implementation of the OpenCL function
 *   }
 * \endcode
 */
OpenCLFunctions::OpenCLFunction::OpenCLFunction(std::string clSourceCode)
    : _code(std::move(clSourceCode))
{
}

/*!
 * Returns the declaration of the function as a string in the format
 * `RetType function_name(ArgType arg, ...);` plus a newline.
 */
std::string OpenCLFunctions::OpenCLFunction::declaration() const
{
    return signature() + ";\n";
}

/*!
 * Returns the full internally stored definition of the function.
 */
std::string OpenCLFunctions::OpenCLFunction::definition() const
{
    return _code;
}

/*!
 * Returns the signature of the function as a string in the format
 * `RetType function_name(ArgType arg, ...)`.
 */
std::string OpenCLFunctions::OpenCLFunction::signature() const
{
    return _code.substr(0, _code.find('{'));
}

} // namespace CTL
