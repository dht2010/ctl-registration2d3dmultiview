#include "projectortest.h"

#include "acquisition/ctsystembuilder.h"
#include "acquisition/trajectories.h"
#include "acquisition/preparesteps.h"
#include "acquisition/systemblueprints.h"
#include "components/allcomponents.h"
#include "io/ctldatabase.h"
#include "processing/errormetrics.h"
#include "projectors/arealfocalspotextension.h"
#include "projectors/poissonnoiseextension.h"
#include "projectors/raycasterprojector.h"
#include "projectors/sfpprojector.h"
#include "projectors/standardpipeline.h"

using namespace CTL;

const bool ENABLE_INTERPOLATION_IN_RAYCASTER = true;
const bool ENABLE_IMAGESUPPORT = true;

void ProjectorTest::initTestCase()
{
    // Volume
    VoxelVolume<float> volume(70, 70, 70);
    volume.setVoxelSize(1.0f, 1.0f, 1.0f);
    volume.fill(0.03f);
    _testVolume = std::move(volume);
}

void ProjectorTest::testPoissonExtension()
{
    poissonSimulation(10, 0.1, 100);
    poissonSimulation(1000, 0.1, 100);
    poissonSimulation(100000, 0.1, 100);

    poissonSimulation(1000, 0.4, 100);
    poissonSimulation(1000, 1.1, 100);

    poissonSimulation(10, 0.1, 200);
    poissonSimulation(1000, 0.1, 200);
    poissonSimulation(100000, 0.1, 200);
}

void ProjectorTest::testSFPProjector()
{
    if(!ENABLE_IMAGESUPPORT)
    {
        qWarning("SFP projector cannot be tested without image support.");
        return;
    }
    // simulate projections
    auto setup = AcquisitionSetup(makeCTSystem<blueprints::GenericCarmCT>(DetectorBinning::Binning4x4), 1);
    setup.applyPreparationProtocol(protocols::ShortScanTrajectory(750.0f));

    auto vol = VoxelVolume<float>::cube(100, 1.0f, 0.02f);

    auto projectorSFP = new OCL::SFPProjector();

    auto projectorRC = new OCL::RayCasterProjector();
    projectorRC->settings().interpolate = false;
    projectorRC->settings().raysPerPixel[0] = 2;
    projectorRC->settings().raysPerPixel[1] = 2;
    projectorRC->settings().raySampling = 0.1f;

    auto projRC  = projectorRC->configureAndProject(setup, vol).first().first();

    // regular versions (ie. atomic int)
    projectorSFP->settings().useAtomicFloat = false;

    // TR
    projectorSFP->settings().footprintType = OCL::SFPProjector::TR;
    auto projSFP = projectorSFP->configureAndProject(setup, vol).first().first();
    QVERIFY2(metric::rL2(projSFP.data(), projRC.data()) < 0.01f, "SFP-TR failed.");

    // TT
    projectorSFP->settings().footprintType = OCL::SFPProjector::TT;
    projSFP = projectorSFP->configureAndProject(setup, vol).first().first();
    QVERIFY2(metric::rL2(projSFP.data(), projRC.data()) < 0.01f, "SFP-TT failed.");

    // Generic TT
    projectorSFP->settings().footprintType = OCL::SFPProjector::TT_Generic;
    projSFP = projectorSFP->configureAndProject(setup, vol).first().first();
    QVERIFY2(metric::rL2(projSFP.data(), projRC.data()) < 0.01f, "SFP-TT_Generic failed.");

    // atomic float versions
    projectorSFP->settings().useAtomicFloat = true;

    // TR
    projectorSFP->settings().footprintType = OCL::SFPProjector::TR;
    projSFP = projectorSFP->configureAndProject(setup, vol).first().first();
    QVERIFY2(metric::rL2(projSFP.data(), projRC.data()) < 0.01f, "SFP-TR (atomic float) failed.");

    // TT
    projectorSFP->settings().footprintType = OCL::SFPProjector::TT;
    projSFP = projectorSFP->configureAndProject(setup, vol).first().first();
    QVERIFY2(metric::rL2(projSFP.data(), projRC.data()) < 0.01f, "SFP-TT (atomic float) failed.");

    // Generic TT
    projectorSFP->settings().footprintType = OCL::SFPProjector::TT_Generic;
    projSFP = projectorSFP->configureAndProject(setup, vol).first().first();
    QVERIFY2(metric::rL2(projSFP.data(), projRC.data()) < 0.01f, "SFP-TT_Generic (atomic float) failed.");
}

void ProjectorTest::testStandardPipelineMoveSyntax()
{
    {
        StandardPipeline s1;
        StandardPipeline s2 = std::move(s1);
    }
    {
        StandardPipeline s1;
        StandardPipeline s2;
        std::swap(s1, s2);
    }
}

void ProjectorTest::poissonSimulation(double meanPhotons,
                                      double projAngle,
                                      uint nbRepetitions) const
{
    CTSystem theSystem;
    auto detector = new FlatPanelDetector(QSize(50, 50), QSizeF(2.0, 2.0), "Flat detector");
    auto gantry = new CarmGantry(1200.0, "Gantry");
    auto source = new XrayLaser(75.0, 1.0, "my tube");


    source->setFocalSpotSize(QSizeF(5.0, 5.0));
    theSystem << detector << gantry << source;

    auto calibPower = meanPhotons / SimpleCTSystem::fromCTSystem(theSystem).photonsPerPixelMean();
    source->setRadiationOutput(calibPower);

    AcquisitionSetup setup(theSystem);
    setup.setNbViews(1);
    setup.applyPreparationProtocol(protocols::ShortScanTrajectory(750.0, projAngle, 0.0));

    auto projector = makeProjector<OCL::RayCasterProjector>();
    projector->settings().interpolate = ENABLE_INTERPOLATION_IN_RAYCASTER;
    projector->settings().useImageTypes = ENABLE_IMAGESUPPORT;

    auto projectionsClean = projector->configureAndProject(setup, _testVolume);

    // pad views with same PrepareSteps
    setup.setNbViews(nbRepetitions);
    auto firstView = setup.view(0);
    for(auto& view : setup.views())
        for(const auto& prep : firstView.prepareSteps())
            view.addPrepareStep(prep);

    auto compoundProjector = std::move(projector) |
                             makeExtension<PoissonNoiseExtension>() |
                             makeExtension<ArealFocalSpotExtension>();

    compoundProjector->setDiscretization(QSize(2, 2));

    // repeatedly compute noisy projections
    auto projsWithNoise = compoundProjector->configureAndProject(setup, _testVolume);

    // ### evaluate results ###
    evaluatePoissonSimulation(projsWithNoise, projectionsClean, setup.system()->photonsPerPixelMean());
}

void ProjectorTest::evaluatePoissonSimulation(const ProjectionData& repeatedProjs,
                                              const ProjectionData& cleanProjections,
                                              double intensity) const
{
    // allowed relative differences w.r.t. mean photon count
    constexpr double requestedPrecisionMeans = 0.01;
    // allowed relative differences w.r.t. mean photon count
    constexpr double requestedPrecisionDiff = 0.05;

    const auto nbRepetitions = repeatedProjs.nbViews();

    // mean photon count (across repetitions) and corresponding variance
    auto repMean = repetitionMean(repeatedProjs, intensity);
    auto repVar = repetitionVariance(repeatedProjs, intensity);

    // difference between mean and variance for each pixel
    // these should be equal in Poisson distributed values
    Chunk2D<double> diffMeanAndVariance = repMean - repVar;

    // average difference between mean and variance across all detector pixels
    double meanDiff = chunkMean(diffMeanAndVariance);

    // variance of differences between mean and variance across all detector pixels
    double diffVar = 0.0;
    const auto& data = diffMeanAndVariance.constData();
    for(auto pix : data)
        diffVar += std::pow((pix - meanDiff), 2.0);
    diffVar /= diffMeanAndVariance.nbElements();

    // average photon count across all detector pixels
    auto meanPhotonsClean
        = chunkMean(transformedToCounts(cleanProjections.view(0).module(0), intensity));
    auto meanPhotonsNoisy = chunkMean(repMean);

    auto differenceStd = std::sqrt(diffVar);

    auto precMeanPhotons = std::fabs(meanPhotonsClean - meanPhotonsNoisy) / meanPhotonsClean;
    auto precDiffMeanVariance = std::fabs(meanDiff) / meanPhotonsClean;

    qInfo().noquote() << "Mean number photons (original | noisy): "
                      << meanPhotonsClean << " | " << meanPhotonsNoisy
                      << " (prec.: " + QString::number(precMeanPhotons) + ")";
    qInfo().noquote() << "Difference mean-variance (" + QString::number(nbRepetitions)
                         + " repetitions): "
                      << meanDiff << " (std: " << differenceStd << ")"
                      << " (prec.: " + QString::number(precDiffMeanVariance) + ")";

    QVERIFY(precMeanPhotons < requestedPrecisionMeans);
    QVERIFY(precDiffMeanVariance < requestedPrecisionDiff);
    QVERIFY(differenceStd < meanPhotonsClean);
}

Chunk2D<double> ProjectorTest::repetitionMean(const ProjectionData& repeatedProjs, double i_0) const
{
    Chunk2D<double> ret(repeatedProjs.dimensions().nbChannels, repeatedProjs.dimensions().nbRows);
    ret.allocateMemory();

    Chunk2D<double> tmp(repeatedProjs.dimensions().nbChannels, repeatedProjs.dimensions().nbRows);
    tmp.setData(std::vector<double>(tmp.nbElements(), 0));

    for(uint rep = 0; rep < repeatedProjs.nbViews(); ++rep)
    {
        tmp += transformedToCounts(repeatedProjs.view(rep).module(0), i_0);
    }

    auto rawRet = ret.rawData();
    auto rawTmp = tmp.rawData();
    for(uint i = 0; i < tmp.nbElements(); ++i)
        rawRet[i] = double(rawTmp[i]) / double(repeatedProjs.nbViews());

    return ret;
}

Chunk2D<double> ProjectorTest::repetitionVariance(const ProjectionData& repeatedProjs,
                                                  double i_0) const
{
    auto mean = repetitionMean(repeatedProjs, i_0);

    Chunk2D<double> tmp(repeatedProjs.dimensions().nbChannels, repeatedProjs.dimensions().nbRows);
    tmp.setData(std::vector<double>(tmp.nbElements(), 0.0));

    auto rawMean = mean.rawData();
    auto rawTmp = tmp.rawData();

    for(uint rep = 0; rep < repeatedProjs.nbViews(); ++rep)
    {
        auto tmpCounts = transformedToCounts(repeatedProjs.view(rep).module(0), i_0);
        auto rawTmpCounts = tmpCounts.rawData();
        for(uint i = 0; i < tmp.nbElements(); ++i)
            rawTmp[i] += pow(rawTmpCounts[i] - rawMean[i], 2.0);
    }

    return tmp / double(repeatedProjs.nbViews());
}

Chunk2D<double> ProjectorTest::transformedToCounts(const SingleViewData::ModuleData& module,
                                                   double i_0) const
{
    Chunk2D<double> ret(module.width(), module.height());
    ret.allocateMemory();

    auto rawDataPtr = ret.rawData();
    const auto& moduleDat = module.constData();
    for(uint pix = 0; pix < module.nbElements(); ++pix)
        rawDataPtr[pix] = i_0 * double(std::exp(-moduleDat[pix]));

    return ret;
}

VolumeData::Dimensions ProjectorTest::toVoxelVolumeDimensions(const io::den::Header& header)
{
    CTL::VolumeData::Dimensions ret;
    ret.x = static_cast<uint>(header.cols);
    ret.y = static_cast<uint>(header.rows);
    ret.z = static_cast<uint>(header.count);
    return ret;
}

template <typename T>
double ProjectorTest::chunkMean(const Chunk2D<T>& chunk) const
{
    double ret = 0.0;
    for(const auto& val : chunk.constData())
        ret += val;

    return ret / double(chunk.nbElements());
}
