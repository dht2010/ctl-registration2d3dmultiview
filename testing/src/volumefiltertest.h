#ifndef VOLUMEFILTERTEST_H
#define VOLUMEFILTERTEST_H

#include "img/voxelvolume.h"
#include "io/jsonserializer.h"
#include "processing/errormetrics.h"

#include <QtTest>

class VolumeFilterTest : public QObject
{
    Q_OBJECT

public:
    VolumeFilterTest();

private Q_SLOTS:
    void initTestCase();
    void testSerialization();

private:
    CTL::VoxelVolume<float> _dummyVol;

    template <class Filter>
    double runSerializationCheck(Filter& filt);
};

template <class Filter>
double VolumeFilterTest::runSerializationCheck(Filter& filt)
{
    auto volCpy1 = _dummyVol;
    filt.filter(volCpy1);

    CTL::JsonSerializer ser;
    ser.serialize(filt, "regul.json");

    auto obj = ser.deserialize<Filter>("regul.json");

    auto volCpy2 = _dummyVol;
    obj->filter(volCpy2);

    return CTL::metric::L2(volCpy1, volCpy2);
}

#endif // VOLUMEFILTERTEST_H
