QT -= gui

CONFIG += console
CONFIG -= app_bundle

DEFINES += QT_NO_DEBUG_OUTPUT

SOURCES += main.cpp

include(../../modules/ctl.pri)
include(../../modules/ctl_ocl.pri)
